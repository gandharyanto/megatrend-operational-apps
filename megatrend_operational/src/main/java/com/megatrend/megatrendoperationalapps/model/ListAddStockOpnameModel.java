package com.megatrend.megatrendoperationalapps.model;

import com.megatrend.megatrendoperationalapps.object.AddStockOpnameObject;

import java.util.List;

public class ListAddStockOpnameModel {
    public List<AddStockOpnameObject> addStockOpnameObjects;

    public List<AddStockOpnameObject> getAddStockOpnameObjects() {
        return addStockOpnameObjects;
    }

    public void setAddStockOpnameObjects(List<AddStockOpnameObject> addStockOpnameObjects) {
        this.addStockOpnameObjects = addStockOpnameObjects;
    }
}
