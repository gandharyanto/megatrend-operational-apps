package com.megatrend.megatrendoperationalapps.model;

import com.google.gson.annotations.SerializedName;
import com.megatrend.megatrendoperationalapps.object.AddStockOpnameObject;

import java.util.List;

/**
 * Created by user on 1/9/2018.
 */

public class ResponsePostProductbyItem {
    @SerializedName("data")
    List<AddStockOpnameObject> data;

    public List<AddStockOpnameObject> getData() {
        return data;
    }

    public void setData(List<AddStockOpnameObject> data) {
        this.data = data;
    }
}
