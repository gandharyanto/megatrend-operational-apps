package com.megatrend.megatrendoperationalapps.model;

import com.google.gson.annotations.SerializedName;
import com.megatrend.megatrendoperationalapps.object.UnitObject;

import java.util.List;

/**
 * Created by user on 1/30/2018.
 */

public class ResponseGetUnit {
    @SerializedName("data")
    private List<UnitObject> data;

    public List<UnitObject> getData() {
        return data;
    }

    public void setData(List<UnitObject> data) {
        this.data = data;
    }
}
