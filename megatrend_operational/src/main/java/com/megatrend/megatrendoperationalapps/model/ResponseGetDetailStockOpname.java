package com.megatrend.megatrendoperationalapps.model;

import com.google.gson.annotations.SerializedName;
import com.megatrend.megatrendoperationalapps.object.DetailStockOpnameObject;

import java.util.List;

/**
 * Created by user on 1/10/2018.
 */

public class ResponseGetDetailStockOpname {
    @SerializedName("msg")
    List<DetailStockOpnameObject> msg;

    public List<DetailStockOpnameObject> getMsg() {
        return msg;
    }

    public void setMsg(List<DetailStockOpnameObject> msg) {
        this.msg = msg;
    }
}
