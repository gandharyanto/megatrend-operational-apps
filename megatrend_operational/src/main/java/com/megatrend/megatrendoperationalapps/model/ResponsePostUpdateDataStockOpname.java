package com.megatrend.megatrendoperationalapps.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 2/6/2018.
 */

public class ResponsePostUpdateDataStockOpname {
    @SerializedName("status")
    String status;
    @SerializedName("msg")
    String msg;
    @SerializedName("type")
    String type;
    @SerializedName("title")
    String title;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
