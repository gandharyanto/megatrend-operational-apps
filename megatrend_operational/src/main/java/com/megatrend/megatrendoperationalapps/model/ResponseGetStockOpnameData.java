package com.megatrend.megatrendoperationalapps.model;

import com.google.gson.annotations.SerializedName;
import com.megatrend.megatrendoperationalapps.object.StockOpnameDataObject;

import java.util.List;

/**
 * Created by user on 1/6/2018.
 */

public class ResponseGetStockOpnameData {
    @SerializedName("draw")
    int draw;
    @SerializedName("recordsTotal")
    int recordsTotal;
    @SerializedName("recordsFiltered")
    int recordsFiltered;
    @SerializedName("data")
    List<StockOpnameDataObject> data;

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public int getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public List<StockOpnameDataObject> getData() {
        return data;
    }

    public void setData(List<StockOpnameDataObject> data) {
        this.data = data;
    }
}
