package com.megatrend.megatrendoperationalapps.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 3/3/2018.
 */

public class ResponsePostLoginUser {
    @SerializedName("idSalesman")
    String idSalesman;
    @SerializedName("idBranch")
    String idBranch;

    public String getIdSalesman() {
        return idSalesman;
    }

    public void setIdSalesman(String idSalesman) {
        this.idSalesman = idSalesman;
    }

    public String getIdBranch() {
        return idBranch;
    }

    public void setIdBranch(String idBranch) {
        this.idBranch = idBranch;
    }
}
