package com.megatrend.megatrendoperationalapps.model;

import com.google.gson.annotations.SerializedName;
import com.megatrend.megatrendoperationalapps.object.BranchObject;

import java.util.List;

/**
 * Created by megatrend on 1/3/2018.
 */

public class ResponseGetBranch {

    @SerializedName("data")
    private List<BranchObject> data;

    public List<BranchObject> getData() {
        return data;
    }

    public void setData(List<BranchObject> data) {
        this.data = data;
    }
}
