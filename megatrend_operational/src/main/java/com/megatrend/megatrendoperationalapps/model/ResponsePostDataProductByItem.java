package com.megatrend.megatrendoperationalapps.model;

import com.google.gson.annotations.SerializedName;
import com.megatrend.megatrendoperationalapps.object.ProductObject;

import java.util.List;

public class ResponsePostDataProductByItem {
    @SerializedName("data")
    List<ProductObject> data;

    public List<ProductObject> getData() {
        return data;
    }

    public void setData(List<ProductObject> data) {
        this.data = data;
    }
}
