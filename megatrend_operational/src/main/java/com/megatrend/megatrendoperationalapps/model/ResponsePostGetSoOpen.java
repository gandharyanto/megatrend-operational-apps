package com.megatrend.megatrendoperationalapps.model;

import com.megatrend.megatrendoperationalapps.object.GetSoOpenObject;

import java.util.List;

public class ResponsePostGetSoOpen {
    private String status;
    private List<GetSoOpenObject> msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<GetSoOpenObject> getGetSoOpenObjects() {
        return msg;
    }

    public void setGetSoOpenObjects(List<GetSoOpenObject> getSoOpenObjects) {
        this.msg = getSoOpenObjects;
    }
}
