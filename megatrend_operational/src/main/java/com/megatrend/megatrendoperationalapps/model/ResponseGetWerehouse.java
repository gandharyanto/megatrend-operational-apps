package com.megatrend.megatrendoperationalapps.model;

import com.google.gson.annotations.SerializedName;
import com.megatrend.megatrendoperationalapps.object.WarehouseObject;

import java.util.List;

/**
 * Created by megatrend on 1/3/2018.
 */

public class ResponseGetWerehouse {

    @SerializedName("data")
    List<WarehouseObject> data;

    public List<WarehouseObject> getData() {
        return data;
    }

    public void setData(List<WarehouseObject> data) {
        this.data = data;
    }
}
