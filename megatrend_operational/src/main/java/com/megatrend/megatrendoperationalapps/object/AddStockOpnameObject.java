package com.megatrend.megatrendoperationalapps.object;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 1/8/2018.
 */

public class AddStockOpnameObject {

    @SerializedName("branch_id")
    String branch_id;

    @SerializedName("warehouse_id")
    String warehouse_id;

    @SerializedName("item_no")
    String item_no;

    @SerializedName("code")
    String code;

    @SerializedName("name")
    String name;

    @SerializedName("stockavl")
    String stockavl;

    @SerializedName("product_id")
    String product_id;

    String date, qty_real, qty_diff, admin, warehouse;

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }

    public String getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(String warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getItem_no() {
        return item_no;
    }

    public void setItem_no(String item_no) {
        this.item_no = item_no;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQty_stock() {
        return stockavl;
    }

    public void setQty_stock(String qty_stock) {
        this.stockavl = qty_stock;
    }

    public String getQty_real() {
        return qty_real;
    }

    public void setQty_real(String qty_real) {
        this.qty_real = qty_real;
    }

    public String getQty_diff() {
        return qty_diff;
    }

    public void setQty_diff(String qty_diff) {
        this.qty_diff = qty_diff;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}
