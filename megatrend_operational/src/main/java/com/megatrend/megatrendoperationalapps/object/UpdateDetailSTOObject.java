package com.megatrend.megatrendoperationalapps.object;

public class UpdateDetailSTOObject {
    String stock_opname_id,
            qty_real,
            item_no,
            name,
            code,
            unit_conversion,
            unit_id,
            unit_detail;

    public String getStock_opname_id() {
        return stock_opname_id;
    }

    public void setStock_opname_id(String stock_opname_id) {
        this.stock_opname_id = stock_opname_id;
    }

    public String getQty_real() {
        return qty_real;
    }

    public void setQty_real(String qty_real) {
        this.qty_real = qty_real;
    }

    public String getItem_no() {
        return item_no;
    }

    public void setItem_no(String item_no) {
        this.item_no = item_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUnit_conversion() {
        return unit_conversion;
    }

    public void setUnit_conversion(String unit_conversion) {
        this.unit_conversion = unit_conversion;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getUnit_detail() {
        return unit_detail;
    }

    public void setUnit_detail(String unit_detail) {
        this.unit_detail = unit_detail;
    }
}
