package com.megatrend.megatrendoperationalapps.object;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 1/10/2018.
 */

public class DetailStockOpnameObject {
    String idTable;
    String unit_detail;
    @SerializedName("id")
    String id;
    @SerializedName("stock_opname_id")
    String stock_opname_id;
    @SerializedName("product_id")
    String product_id;
    @SerializedName("qty")
    String qty;
    @SerializedName("current_qty")
    String qty_real;
    @SerializedName("difference")
    String deference;
    @SerializedName("item_no")
    String item_no;
    @SerializedName("deleted_at")
    String deleted_at;
    @SerializedName("name")
    String name;
    @SerializedName("code")
    String code;
    @SerializedName("warehouse_id")
    String warehouse_id;
    @SerializedName("branch_id")
    String branch_id;
    @SerializedName("unit_id")
    String unit_id;
    @SerializedName("qty_conversion")
    String unit_conversion;
    @SerializedName("total_qty")
    String total_qty;
    @SerializedName("unitname")
    String unitname;

    public String getUnit_detail() {
        return unit_detail;
    }

    public void setUnit_detail(String unit_detail) {
        this.unit_detail = unit_detail;
    }

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getUnit_conversion() {
        return unit_conversion;
    }

    public void setUnit_conversion(String unit_conversion) {
        this.unit_conversion = unit_conversion;
    }

    public String getTotal_qty() {
        return total_qty;
    }

    public void setTotal_qty(String total_qty) {
        this.total_qty = total_qty;
    }

    public String getIdTable() {
        return idTable;
    }

    public void setIdTable(String idTable) {
        this.idTable = idTable;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStock_opname_id() {
        return stock_opname_id;
    }

    public void setStock_opname_id(String stock_opname_id) {
        this.stock_opname_id = stock_opname_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getQty_real() {
        return qty_real;
    }

    public void setQty_real(String qty_real) {
        this.qty_real = qty_real;
    }

    public String getDeference() {
        return deference;
    }

    public void setDeference(String deference) {
        this.deference = deference;
    }

    public String getItem_no() {
        return item_no;
    }

    public void setItem_no(String item_no) {
        this.item_no = item_no;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(String warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }
}
