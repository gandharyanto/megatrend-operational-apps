package com.megatrend.megatrendoperationalapps.object;

public class ProductObject {
    String product_id, item_no, code, name, stockavl;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getItem_no() {
        return item_no;
    }

    public void setItem_no(String item_no) {
        this.item_no = item_no;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStockavl() {
        return stockavl;
    }

    public void setStockavl(String stockavl) {
        this.stockavl = stockavl;
    }
}
