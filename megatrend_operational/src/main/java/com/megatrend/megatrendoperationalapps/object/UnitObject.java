package com.megatrend.megatrendoperationalapps.object;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 1/30/2018.
 */

public class UnitObject {
    @SerializedName("unit_name")
    private String unit_name;
    @SerializedName("unit_id")
    private String unit_id;
    @SerializedName("qtykali")
    private String qtykali;
    @SerializedName("code")
    private String code;

    public String getUnit_name() {
        return unit_name;
    }

    public void setUnit_name(String unit_name) {
        this.unit_name = unit_name;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getQtykali() {
        return qtykali;
    }

    public void setQtykali(String qtykali) {
        this.qtykali = qtykali;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
