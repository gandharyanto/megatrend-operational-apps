package com.megatrend.megatrendoperationalapps.object;

/**
 * Created by user on 1/25/2018.
 */

public class SubmitStockOpnameObject {
    String date;
    String branch_id;
    String warehouse_id;
    String updated_by;
    String productid;
    String qty;
    String qtyreal;
    String item_no;
    String unit_id;
    String unit_conv;
    String aso_api_id;
    String dso_api_id;
    String employee_id;

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getAso_api_id() {
        return aso_api_id;
    }

    public void setAso_api_id(String aso_api_id) {
        this.aso_api_id = aso_api_id;
    }

    public String getDso_api_id() {
        return dso_api_id;
    }

    public void setDso_api_id(String dso_api_id) {
        this.dso_api_id = dso_api_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }

    public String getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(String warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getQtyreal() {
        return qtyreal;
    }

    public void setQtyreal(String qtyreal) {
        this.qtyreal = qtyreal;
    }

    public String getItem_no() {
        return item_no;
    }

    public void setItem_no(String item_no) {
        this.item_no = item_no;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getUnit_conv() {
        return unit_conv;
    }

    public void setUnit_conv(String unit_conv) {
        this.unit_conv = unit_conv;
    }
}
