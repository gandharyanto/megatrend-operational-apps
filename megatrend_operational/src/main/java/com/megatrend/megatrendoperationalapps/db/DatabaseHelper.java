package com.megatrend.megatrendoperationalapps.db;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Environment;

import com.megatrend.megatrendoperationalapps.object.AddStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.DetailStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.SoOpenDetailObject;
import com.megatrend.megatrendoperationalapps.object.StockOpnameDataObject;
import com.megatrend.megatrendoperationalapps.object.SubmitStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.UpdateDetailSTOObject;
import com.megatrend.megatrendoperationalapps.object.WarehouseObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by user on 1/11/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    //TABLE NAME
    private static final String ALL_STOCK_OPNAME_TABLE = "ALL_STOCK_OPNAME";
    private static final String DETAIL_STOCK_OPNAME_TABLE = "DETAIL_STOCK_OPNAME";
    private static final String WAREHOUSE_TABLE = "WAREHOUSE";
    private static final String SO_OPEN_TABLE = "SO_OPEN";
    private static final String PRODUCT_TABLE = "PRODUCT";

    //ALL_STOCK_OPNAME_TABLE COLUMNS
    private static final String ASO_COL_ID = "id";
    private static final String ASO_COL_CODE = "code";
    private static final String ASO_COL_API_ID = "api_id";
    private static final String ASO_COL_DATE = "date";
    private static final String ASO_COL_CREATED_AT = "created_at";
    private static final String ASO_COL_UPDATED_BY = "updated_by";
    private static final String ASO_COL_UPDATED_AT = "updated_at";
    private static final String ASO_COL_WAREHOUSE_ID = "warehouse_id";
    private static final String ASO_COL_BRANCH_ID = "branch_id";
    private static final String ASO_COL_DELETED_AT = "deleted_at";
    private static final String ASO_COL_WAREHOUSE = "warehouse";
    private static final String ASO_COL_ISAPI = "isapi";

    //DETAIL_STOCK_OPNAME_TABLE COLUMNS
    private static final String DSO_COL_ID = "id";
    private static final String DSO_COL_API_ID = "api_id";
    private static final String DSO_COL_STOCK_OPNAME_ID = "stock_opname_id";
    private static final String DSO_COL_PRODUCT_ID = "product_id";
    private static final String DSO_COL_QTY = "qty";
    private static final String DSO_COL_QTY_REAL = "qty_real";
    private static final String DSO_COL_DIFFERENCE = "difference";
    private static final String DSO_COL_TOT = "total_qty";
    private static final String DSO_COL_ITEM_NO = "item_no";
    private static final String DSO_COL_DELETED_AT = "deleted_at";
    private static final String DSO_COL_NAME = "name";
    private static final String DSO_COL_CODE = "code";
    private static final String DSO_COL_WAREHOUSE_ID = "warehouse_id";
    private static final String DSO_COL_EMPLOYEE_ID = "employee_id";
    private static final String DSO_COL_BRANCH_ID = "branch_id";
    private static final String DSO_COL_UNIT_CONV = "unit_conversion";
    private static final String DSO_COL_UNIT_ID = "unit_id";
    private static final String DSO_COL_UNIT_DETAIL = "unit_detail";
    private static final String DSO_COL_ISAPI = "isapi";

    //WAREHOUSE_TABLE COLUMNS
    private static final String WH_COL_ID = "id";
    private static final String WH_COL_API_ID = "api_id";
    private static final String WH_COL_BRANCH_ID = "branch_id";
    private static final String WH_COL_CODE = "code";
    private static final String WH_COL_NAME = "name";
    private static final String WH_COL_DESCRIPTION = "description";
    private static final String WH_COL_EMPLOYEE_ID = "employee_id";
    private static final String WH_COL_AREA_POST_CODE_ID = "area_post_code_id";
    private static final String WH_COL_ADDRESS = "address";

    //SO_OPEN_TABLE COLUMNS
    private static final String SOO_COL_ID = "id";
    private static final String SOO_COL_API_ID = "api_id";
    private static final String SOO_COL_CODE = "code";
    private static final String SOO_COL_STATUS = "status";
    private static final String SOO_COL_DATE = "date";

    //PRODUCT_TABLE COLUMNS
    private static final String PRO_COL_ID = "id";
    private static final String PRO_COL_PRODUCT_ID = "product_id";
    private static final String PRO_COL_ITEM_NO = "item_no";
    private static final String PRO_COL_CODE = "code";
    private static final String PRO_COL_NAME = "name";
    private static final String PRO_COL_STOCKAVL = "stockavl";

    // Database Information
    private static final String DB_NAME = "MEGATREND_APPS.DB";

    // database version
    private static final int DB_VERSION = 8;

    //CREATE TABLE ALL_STOCK_OPNAME_TABLE
    private static final String CREATE_TABLE_ALL_STOCK_OPNAME = "CREATE TABLE " + ALL_STOCK_OPNAME_TABLE + "(" +
            ASO_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ASO_COL_API_ID + " TEXT DEFAULT NULL, " +
            ASO_COL_CODE + " TEXT DEFAULT NULL, " +
            ASO_COL_DATE + " TEXT DEFAULT NULL, " +
            ASO_COL_CREATED_AT + " TEXT DEFAULT NULL, " +
            ASO_COL_UPDATED_BY + " TEXT DEFAULT NULL, " +
            ASO_COL_UPDATED_AT + " TEXT DEFAULT NULL, " +
            ASO_COL_WAREHOUSE_ID + " TEXT DEFAULT NULL, " +
            ASO_COL_BRANCH_ID + " TEXT DEFAULT NULL, " +
            ASO_COL_DELETED_AT + " TEXT DEFAULT NULL, " +
            ASO_COL_WAREHOUSE + " TEXT DEFAULT NULL, " +
            ASO_COL_ISAPI + " TEXT DEFAULT NULL " +
            ");";

    //CREATE TABLE DETAIL_STOCK_OPNAME_TABLE
    private static final String CREATE_TABLE_DETAIL_STOCK_OPNAME = "CREATE TABLE " + DETAIL_STOCK_OPNAME_TABLE + "(" +
            DSO_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DSO_COL_API_ID + " TEXT DEFAULT NULL, " +
            DSO_COL_STOCK_OPNAME_ID + " TEXT DEFAULT NULL, " +
            DSO_COL_PRODUCT_ID + " TEXT DEFAULT NULL, " +
            DSO_COL_QTY + " TEXT DEFAULT NULL, " +
            DSO_COL_QTY_REAL + " TEXT DEFAULT NULL, " +
            DSO_COL_DIFFERENCE + " TEXT DEFAULT NULL, " +
            DSO_COL_TOT + " TEXT DEFAULT NULL, " +
            DSO_COL_ITEM_NO + " TEXT DEFAULT NULL, " +
            DSO_COL_DELETED_AT + " TEXT DEFAULT NULL, " +
            DSO_COL_NAME + " TEXT DEFAULT NULL, " +
            DSO_COL_CODE + " TEXT DEFAULT NULL, " +
            DSO_COL_WAREHOUSE_ID + " TEXT DEFAULT NULL, " +
            DSO_COL_EMPLOYEE_ID + " TEXT DEFAULT NULL, " +
            DSO_COL_BRANCH_ID + " TEXT DEFAULT NULL, " +
            DSO_COL_UNIT_CONV + " TEXT DEFAULT NULL, " +
            DSO_COL_UNIT_ID + " TEXT DEFAULT NULL, " +
            DSO_COL_UNIT_DETAIL + " TEXT DEFAULT NULL, " +
            DSO_COL_ISAPI + " TEXT DEFAULT NULL " +
            ");";

    //CREATE TABLE DETAIL_STOCK_OPNAME_TABLE
    private static final String CREATE_TABLE_WAREHOUSE = "CREATE TABLE " + WAREHOUSE_TABLE + "(" +
            WH_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            WH_COL_API_ID + " TEXT DEFAULT NULL, " +
            WH_COL_BRANCH_ID + " TEXT DEFAULT NULL, " +
            WH_COL_CODE + " TEXT DEFAULT NULL, " +
            WH_COL_NAME + " TEXT DEFAULT NULL, " +
            WH_COL_DESCRIPTION + " TEXT DEFAULT NULL, " +
            WH_COL_EMPLOYEE_ID + " TEXT DEFAULT NULL, " +
            WH_COL_AREA_POST_CODE_ID + " TEXT DEFAULT NULL, " +
            WH_COL_ADDRESS + " TEXT DEFAULT NULL" +
            ");";

    //CREATE TABLE SO_OPEN_TABLE
    private static final String CREATE_TABLE_SO_OPEN = "CREATE TABLE " + SO_OPEN_TABLE + "(" +
            SOO_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            SOO_COL_API_ID + " TEXT DEFAULT NULL, " +
            SOO_COL_CODE + " TEXT DEFAULT NULL," +
            SOO_COL_STATUS + " TEXT DEFAULT NULL," +
            SOO_COL_DATE + " TEXT DEFAULT NULL" +
            ");";

    //CREATE TABLE PRODUCT_TABLE
    private static final String CREATE_TABLE_PRODUCT = "CREATE TABLE " + PRODUCT_TABLE + "(" +
            PRO_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            PRO_COL_PRODUCT_ID + " TEXT DEFAULT NULL, " +
            PRO_COL_ITEM_NO + " TEXT DEFAULT NULL," +
            PRO_COL_CODE + " TEXT DEFAULT NULL," +
            PRO_COL_NAME + " TEXT DEFAULT NULL," +
            PRO_COL_STOCKAVL + " TEXT DEFAULT NULL" +
            ");";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        Timber.d(CREATE_TABLE_ALL_STOCK_OPNAME);
        Timber.d(CREATE_TABLE_DETAIL_STOCK_OPNAME);
        Timber.d(CREATE_TABLE_WAREHOUSE);
        Timber.d(CREATE_TABLE_SO_OPEN);
        Timber.d(CREATE_TABLE_PRODUCT);
    }

    public static void exportDB(Activity activity) {
        // TODO Auto-generated method stub
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + "com.megatrend.megatrendoperationalapps"
                        + "//databases//" + "MEGATREND_APPS.DB";
                String backupDBPath = "/Download/MEGATRENDAPPS.txt";
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();

                final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                File file = new File(Environment.getExternalStorageDirectory() + "/Download", "MEGATRENDAPPS.txt");
                final Uri contentUri = Uri.fromFile(file);
                scanIntent.setData(contentUri);
                activity.sendBroadcast(scanIntent);
                Timber.d(backupDBPath);
            }
        } catch (Exception e) {
            //Toast.makeText(act, e.getMessage(), Toast.LENGTH_LONG).show();
            Timber.e("gagal export (" + e.getMessage() + ")");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ALL_STOCK_OPNAME);
        db.execSQL(CREATE_TABLE_DETAIL_STOCK_OPNAME);
        db.execSQL(CREATE_TABLE_WAREHOUSE);
        db.execSQL(CREATE_TABLE_SO_OPEN);
        db.execSQL(CREATE_TABLE_PRODUCT);
        Timber.d("onCreateDB");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ALL_STOCK_OPNAME_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DETAIL_STOCK_OPNAME_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + WAREHOUSE_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + SO_OPEN_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + PRODUCT_TABLE);
        onCreate(db);
        Timber.d("onUpgradeDB");
    }

    public void dropAll_Stock_Opname_Table() {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DROP TABLE IF EXISTS '" + ALL_STOCK_OPNAME_TABLE + "'");
        database.execSQL(CREATE_TABLE_ALL_STOCK_OPNAME);
        Timber.d("dropALL_STOCK_OPNAME_TABLE");
    }

    public void dropDetail_Stock_Opname_Table() {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DROP TABLE IF EXISTS '" + DETAIL_STOCK_OPNAME_TABLE + "'");
        database.execSQL(CREATE_TABLE_DETAIL_STOCK_OPNAME);
        Timber.d("dropDetail_Stock_Opname_Table");
    }

    public void dropAll_Table() {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DROP TABLE IF EXISTS '" + ALL_STOCK_OPNAME_TABLE + "'");
        database.execSQL("DROP TABLE IF EXISTS '" + DETAIL_STOCK_OPNAME_TABLE + "'");
        database.execSQL("DROP TABLE IF EXISTS " + WAREHOUSE_TABLE);
        database.execSQL("DROP TABLE IF EXISTS " + SO_OPEN_TABLE);
        database.execSQL(CREATE_TABLE_ALL_STOCK_OPNAME);
        database.execSQL(CREATE_TABLE_DETAIL_STOCK_OPNAME);
        database.execSQL(CREATE_TABLE_WAREHOUSE);
        database.execSQL(CREATE_TABLE_SO_OPEN);
        Timber.d("dropAll_Table");
    }

    public void dropProduct() {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DROP TABLE IF EXISTS " + PRODUCT_TABLE);
        database.execSQL(CREATE_TABLE_PRODUCT);
        Timber.d("dropProduct");
    }

    public void deleteCloseSTO(String code) throws SQLException {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DELETE FROM " + SO_OPEN_TABLE + " WHERE " + SOO_COL_CODE + " = '" + code + "'");
    }

    public void deleteDetailSTO(String code) throws SQLException {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DELETE FROM " + DETAIL_STOCK_OPNAME_TABLE + " WHERE " + DSO_COL_CODE + " = '" + code + "'");
    }

    public void insertAllStockOpname(String api_id, String date, String created_at, String updated_by,
                                     String updated_at, String warehouse_id, String branch_id,
                                     String deleted_at, String warehouse, String isApi, String code) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.ASO_COL_API_ID, api_id);
        contentValues.put(DatabaseHelper.ASO_COL_CODE, code);
        contentValues.put(DatabaseHelper.ASO_COL_DATE, date);
        contentValues.put(DatabaseHelper.ASO_COL_CREATED_AT, created_at);
        contentValues.put(DatabaseHelper.ASO_COL_UPDATED_BY, updated_by);
        contentValues.put(DatabaseHelper.ASO_COL_UPDATED_AT, updated_at);
        contentValues.put(DatabaseHelper.ASO_COL_WAREHOUSE_ID, warehouse_id);
        contentValues.put(DatabaseHelper.ASO_COL_BRANCH_ID, branch_id);
        contentValues.put(DatabaseHelper.ASO_COL_DELETED_AT, deleted_at);
        contentValues.put(DatabaseHelper.ASO_COL_WAREHOUSE, warehouse);
        contentValues.put(DatabaseHelper.ASO_COL_ISAPI, isApi);
        long id = db.insertWithOnConflict(ALL_STOCK_OPNAME_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        Timber.d("%s", id);
    }

    public void insertDetailStockOpname(String api_id, String stock_opname_id, String product_id, String qty,
                                        String qty_real, String difference, String item_no,
                                        String deleted_at, String name, String code, String warehouse_id, String employee_id,
                                        String branch_id, String unit_id, String unit_conv, String unit_detail, String isApi) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.DSO_COL_API_ID, api_id);
        contentValues.put(DatabaseHelper.DSO_COL_STOCK_OPNAME_ID, stock_opname_id);
        contentValues.put(DatabaseHelper.DSO_COL_PRODUCT_ID, product_id);
        contentValues.put(DatabaseHelper.DSO_COL_QTY, qty);
        contentValues.put(DatabaseHelper.DSO_COL_QTY_REAL, qty_real);
        contentValues.put(DatabaseHelper.DSO_COL_DIFFERENCE, difference);
        contentValues.put(DatabaseHelper.DSO_COL_ITEM_NO, item_no);
        contentValues.put(DatabaseHelper.DSO_COL_DELETED_AT, deleted_at);
        contentValues.put(DatabaseHelper.DSO_COL_TOT, deleted_at);
        contentValues.put(DatabaseHelper.DSO_COL_NAME, name);
        contentValues.put(DatabaseHelper.DSO_COL_CODE, code);
        contentValues.put(DatabaseHelper.DSO_COL_WAREHOUSE_ID, warehouse_id);
        contentValues.put(DatabaseHelper.DSO_COL_EMPLOYEE_ID, employee_id);
        contentValues.put(DatabaseHelper.DSO_COL_BRANCH_ID, branch_id);
        contentValues.put(DatabaseHelper.DSO_COL_UNIT_ID, unit_id);
        contentValues.put(DatabaseHelper.DSO_COL_UNIT_CONV, unit_conv);
        contentValues.put(DatabaseHelper.DSO_COL_UNIT_DETAIL, unit_detail);
        contentValues.put(DatabaseHelper.DSO_COL_ISAPI, isApi);
        long id = db.insertWithOnConflict(DETAIL_STOCK_OPNAME_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        Timber.d("%s", id);
    }

    public void insertAddDetailStockOpname(String api_id, String stock_opname_id, String product_id, String qty,
                                           String qty_real, String difference, String item_no,
                                           String deleted_at, String name, String code, String warehouse_id, String employee_id,
                                           String branch_id, String unit_id, String unit_conv, String unit_detail, String isApi) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.DSO_COL_API_ID, api_id);
        contentValues.put(DatabaseHelper.DSO_COL_STOCK_OPNAME_ID, stock_opname_id);
        contentValues.put(DatabaseHelper.DSO_COL_PRODUCT_ID, product_id);
        contentValues.put(DatabaseHelper.DSO_COL_QTY, qty);
        contentValues.put(DatabaseHelper.DSO_COL_QTY_REAL, qty_real);
        contentValues.put(DatabaseHelper.DSO_COL_DIFFERENCE, difference);
        contentValues.put(DatabaseHelper.DSO_COL_ITEM_NO, item_no);
        contentValues.put(DatabaseHelper.DSO_COL_DELETED_AT, deleted_at);
        contentValues.put(DatabaseHelper.DSO_COL_NAME, name);
        contentValues.put(DatabaseHelper.DSO_COL_CODE, code);
        contentValues.put(DatabaseHelper.DSO_COL_WAREHOUSE_ID, warehouse_id);
        contentValues.put(DatabaseHelper.DSO_COL_EMPLOYEE_ID, employee_id);
        contentValues.put(DatabaseHelper.DSO_COL_BRANCH_ID, branch_id);
        contentValues.put(DatabaseHelper.DSO_COL_UNIT_ID, unit_id);
        contentValues.put(DatabaseHelper.DSO_COL_UNIT_CONV, unit_conv);
        contentValues.put(DatabaseHelper.DSO_COL_UNIT_DETAIL, unit_detail);
        contentValues.put(DatabaseHelper.DSO_COL_ISAPI, isApi);
        long id = db.insertWithOnConflict(DETAIL_STOCK_OPNAME_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        Timber.d("%s", id);
    }

    public void insertWarehouse(String api_id, String branch_id, String code, String name,
                                String desc, String emp, String add, String area) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.WH_COL_API_ID, api_id);
        contentValues.put(DatabaseHelper.WH_COL_BRANCH_ID, branch_id);
        contentValues.put(DatabaseHelper.WH_COL_CODE, code);
        contentValues.put(DatabaseHelper.WH_COL_NAME, name);
        contentValues.put(DatabaseHelper.WH_COL_DESCRIPTION, desc);
        contentValues.put(DatabaseHelper.WH_COL_EMPLOYEE_ID, emp);
        contentValues.put(DatabaseHelper.WH_COL_ADDRESS, add);
        contentValues.put(DatabaseHelper.WH_COL_AREA_POST_CODE_ID, area);
        long id = db.insertWithOnConflict(WAREHOUSE_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        Timber.d("%s", id);
    }

    public void insertSOOpen(String api_id, String code, String status, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.SOO_COL_API_ID, api_id);
        contentValues.put(DatabaseHelper.SOO_COL_CODE, code);
        contentValues.put(DatabaseHelper.SOO_COL_STATUS, status);
        contentValues.put(DatabaseHelper.SOO_COL_DATE, date);
        long id = db.insertWithOnConflict(SO_OPEN_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        Timber.d("%s", id);
    }

    public void insertProduct(String product_id, String item_no, String code, String name, String stockavl) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.PRO_COL_PRODUCT_ID, product_id);
        contentValues.put(DatabaseHelper.PRO_COL_ITEM_NO, item_no);
        contentValues.put(DatabaseHelper.PRO_COL_CODE, code);
        contentValues.put(DatabaseHelper.PRO_COL_NAME, name);
        contentValues.put(DatabaseHelper.PRO_COL_STOCKAVL, stockavl);
        long id = db.insertWithOnConflict(PRODUCT_TABLE, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
        Timber.d("%s", id);
    }

    public void updateAddDetailStockOpname(String qty_real, String code, String unit_conv, String unit_det, String unit_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DSO_COL_QTY_REAL, qty_real);
        values.put(DSO_COL_CODE, code);
        values.put(DSO_COL_UNIT_CONV, unit_conv);
        values.put(DSO_COL_UNIT_DETAIL, unit_det);
        values.put(DSO_COL_UNIT_ID, unit_id);
        db.update(DETAIL_STOCK_OPNAME_TABLE, values, DSO_COL_CODE + " = ?", new String[]{code});
    }

    public List<SoOpenDetailObject> getTableSoOpen() {
        List<SoOpenDetailObject> soOpenDetailObjectList = new ArrayList<>();

        /*String selectQuery = "select SO_OPEN.*, ALL_STOCK_OPNAME.warehouse, ALL_STOCK_OPNAME.warehouse_id, WAREHOUSE.employee_id from SO_OPEN " +
        "left join ALL_STOCK_OPNAME on SO_OPEN.api_id = ALL_STOCK_OPNAME.api_id " +
        "left join WAREHOUSE on ALL_STOCK_OPNAME.warehouse_id = WAREHOUSE.api_id";*/

        String selectQuery = "select " + SO_OPEN_TABLE + "." + SOO_COL_API_ID +
                "," + SO_OPEN_TABLE + "." + SOO_COL_CODE +
                "," + ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_WAREHOUSE +
                "," + ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_WAREHOUSE_ID +
                "," + WAREHOUSE_TABLE + "." + WH_COL_EMPLOYEE_ID +
                " from " + SO_OPEN_TABLE + " left join " + ALL_STOCK_OPNAME_TABLE + " on " +
                SO_OPEN_TABLE + "." + SOO_COL_API_ID + " = " +
                ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_API_ID + " left join " + WAREHOUSE_TABLE +
                " on " + ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_WAREHOUSE_ID + " = " +
                WAREHOUSE_TABLE + "." + WH_COL_API_ID;

        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                SoOpenDetailObject soOpenDetailObject = new SoOpenDetailObject();
                soOpenDetailObject.setApi_id(c.getString(c.getColumnIndex(SOO_COL_API_ID)));
                soOpenDetailObject.setWarehouse(c.getString(c.getColumnIndex(ASO_COL_WAREHOUSE)));
                soOpenDetailObject.setWarehouse_id(c.getString(c.getColumnIndex(ASO_COL_WAREHOUSE_ID)));
                soOpenDetailObject.setEmployee_id(c.getString(c.getColumnIndex(WH_COL_EMPLOYEE_ID)));
                soOpenDetailObject.setCode(c.getString(c.getColumnIndex(ASO_COL_CODE)));
                soOpenDetailObjectList.add(soOpenDetailObject);
            } while (c.moveToNext());
        }

        c.close();
        return soOpenDetailObjectList;
    }

    public UpdateDetailSTOObject getUpdateDetailSTO(String code) {
        UpdateDetailSTOObject updateDetailSTOObject = new UpdateDetailSTOObject();
        String selectQuery = "select " + DSO_COL_STOCK_OPNAME_ID + ","
                + DSO_COL_QTY_REAL + "," + DSO_COL_ITEM_NO + "," + DSO_COL_NAME + ","
                + DSO_COL_CODE + ","
                + DSO_COL_UNIT_CONV + ","
                + DSO_COL_UNIT_ID + ","
                + DSO_COL_UNIT_DETAIL + " from " + DETAIL_STOCK_OPNAME_TABLE + " where " + DSO_COL_CODE + " = '" + code + "'";

        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                updateDetailSTOObject.setStock_opname_id(c.getString(c.getColumnIndex(DSO_COL_STOCK_OPNAME_ID)));
                updateDetailSTOObject.setQty_real(c.getString(c.getColumnIndex(DSO_COL_QTY_REAL)));
                updateDetailSTOObject.setItem_no(c.getString(c.getColumnIndex(DSO_COL_ITEM_NO)));
                updateDetailSTOObject.setName(c.getString(c.getColumnIndex(DSO_COL_NAME)));
                updateDetailSTOObject.setCode(c.getString(c.getColumnIndex(DSO_COL_CODE)));
                updateDetailSTOObject.setUnit_conversion(c.getString(c.getColumnIndex(DSO_COL_UNIT_CONV)));
                updateDetailSTOObject.setUnit_id(c.getString(c.getColumnIndex(DSO_COL_UNIT_ID)));
                updateDetailSTOObject.setUnit_detail(c.getString(c.getColumnIndex(DSO_COL_UNIT_DETAIL)));
            } while (c.moveToNext());
        }

        c.close();

        return updateDetailSTOObject;
    }

    public List<AddStockOpnameObject> getTableProductByCode(String code) {
        List<AddStockOpnameObject> productObjectList = new ArrayList<>();

        /*String selectQuery = "select SO_OPEN.*, ALL_STOCK_OPNAME.warehouse, ALL_STOCK_OPNAME.warehouse_id, WAREHOUSE.employee_id from SO_OPEN " +
        "left join ALL_STOCK_OPNAME on SO_OPEN.api_id = ALL_STOCK_OPNAME.api_id " +
        "left join WAREHOUSE on ALL_STOCK_OPNAME.warehouse_id = WAREHOUSE.api_id";*/

        String selectQuery = "select * from " + PRODUCT_TABLE + " where " + PRO_COL_CODE + " = '" + code + "'";

        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                AddStockOpnameObject productObject = new AddStockOpnameObject();
                productObject.setProduct_id(c.getString(c.getColumnIndex(PRO_COL_PRODUCT_ID)));
                productObject.setCode(c.getString(c.getColumnIndex(PRO_COL_CODE)));
                productObject.setItem_no(c.getString(c.getColumnIndex(PRO_COL_ITEM_NO)));
                productObject.setName(c.getString(c.getColumnIndex(PRO_COL_NAME)));
                productObject.setQty_stock(c.getString(c.getColumnIndex(PRO_COL_STOCKAVL)));
                productObjectList.add(productObject);
            } while (c.moveToNext());
        }

        c.close();
        return productObjectList;
    }

    public List<AddStockOpnameObject> getTableProduct() {
        List<AddStockOpnameObject> productObjectList = new ArrayList<>();

        /*String selectQuery = "select SO_OPEN.*, ALL_STOCK_OPNAME.warehouse, ALL_STOCK_OPNAME.warehouse_id, WAREHOUSE.employee_id from SO_OPEN " +
        "left join ALL_STOCK_OPNAME on SO_OPEN.api_id = ALL_STOCK_OPNAME.api_id " +
        "left join WAREHOUSE on ALL_STOCK_OPNAME.warehouse_id = WAREHOUSE.api_id";*/

        String selectQuery = "select * from " + PRODUCT_TABLE;

        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                AddStockOpnameObject productObject = new AddStockOpnameObject();
                productObject.setProduct_id(c.getString(c.getColumnIndex(PRO_COL_PRODUCT_ID)));
                productObject.setCode(c.getString(c.getColumnIndex(PRO_COL_CODE)));
                productObject.setItem_no(c.getString(c.getColumnIndex(PRO_COL_ITEM_NO)));
                productObject.setName(c.getString(c.getColumnIndex(PRO_COL_NAME)));
                productObject.setQty_stock(c.getString(c.getColumnIndex(PRO_COL_STOCKAVL)));
                productObjectList.add(productObject);
            } while (c.moveToNext());
        }

        c.close();
        return productObjectList;
    }

    public List<StockOpnameDataObject> getTableStockOpnameData() {
        List<StockOpnameDataObject> stockOpnameDataObjectList = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + ALL_STOCK_OPNAME_TABLE + " ORDER BY " + ASO_COL_API_ID + " DESC";

        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                StockOpnameDataObject stockOpnameDataObject = new StockOpnameDataObject();
                stockOpnameDataObject.setTable_id(c.getString(c.getColumnIndex(ASO_COL_ID)));
                stockOpnameDataObject.setId(c.getString(c.getColumnIndex(ASO_COL_API_ID)));
                stockOpnameDataObject.setDate(c.getString(c.getColumnIndex(ASO_COL_DATE)));
                stockOpnameDataObject.setCreated_at(c.getString(c.getColumnIndex(ASO_COL_CREATED_AT)));
                stockOpnameDataObject.setUpdated_by(c.getString(c.getColumnIndex(ASO_COL_UPDATED_BY)));
                stockOpnameDataObject.setUpdated_at(c.getString(c.getColumnIndex(ASO_COL_UPDATED_AT)));
                stockOpnameDataObject.setWarehouse_id(c.getString(c.getColumnIndex(ASO_COL_WAREHOUSE_ID)));
                stockOpnameDataObject.setBranch_id(c.getString(c.getColumnIndex(ASO_COL_BRANCH_ID)));
                stockOpnameDataObject.setDeleted_at(c.getString(c.getColumnIndex(ASO_COL_DELETED_AT)));
                stockOpnameDataObject.setWarehouse(c.getString(c.getColumnIndex(ASO_COL_WAREHOUSE)));
                stockOpnameDataObjectList.add(stockOpnameDataObject);
            } while (c.moveToNext());
        }

        c.close();
        return stockOpnameDataObjectList;
    }

    public String[][] getTableStockOpnameDataArray() {
        ArrayList<StockOpnameDataObject> stockOpnameDataObjectList = new ArrayList<>();
        String[][] dataList;

        String selectQuery = "SELECT * FROM " + ALL_STOCK_OPNAME_TABLE + " ORDER BY " + ASO_COL_API_ID + " DESC";

        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                StockOpnameDataObject stockOpnameDataObject = new StockOpnameDataObject();
                stockOpnameDataObject.setTable_id(c.getString(c.getColumnIndex(ASO_COL_ID)));
                stockOpnameDataObject.setId(c.getString(c.getColumnIndex(ASO_COL_API_ID)));
                stockOpnameDataObject.setCode(c.getString(c.getColumnIndex(ASO_COL_CODE)));
                stockOpnameDataObject.setDate(c.getString(c.getColumnIndex(ASO_COL_DATE)));
                stockOpnameDataObject.setCreated_at(c.getString(c.getColumnIndex(ASO_COL_CREATED_AT)));
                stockOpnameDataObject.setUpdated_by(c.getString(c.getColumnIndex(ASO_COL_UPDATED_BY)));
                stockOpnameDataObject.setUpdated_at(c.getString(c.getColumnIndex(ASO_COL_UPDATED_AT)));
                stockOpnameDataObject.setWarehouse_id(c.getString(c.getColumnIndex(ASO_COL_WAREHOUSE_ID)));
                stockOpnameDataObject.setBranch_id(c.getString(c.getColumnIndex(ASO_COL_BRANCH_ID)));
                stockOpnameDataObject.setDeleted_at(c.getString(c.getColumnIndex(ASO_COL_DELETED_AT)));
                stockOpnameDataObject.setWarehouse(c.getString(c.getColumnIndex(ASO_COL_WAREHOUSE)));
                stockOpnameDataObjectList.add(stockOpnameDataObject);
            } while (c.moveToNext());
        }

        c.close();

        dataList = new String[stockOpnameDataObjectList.size()][4];
        for (int i = 0; i < stockOpnameDataObjectList.size(); i++) {

            StockOpnameDataObject s = stockOpnameDataObjectList.get(i);

            dataList[i][0] = s.getCode();
            dataList[i][1] = s.getDate();
            dataList[i][2] = s.getWarehouse();
            dataList[i][3] = s.getUpdated_by();
        }
        return dataList;
    }

    public List<WarehouseObject> getWarehouseData(String branch) {
        List<WarehouseObject> warehouseObjectArrayList = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + WAREHOUSE_TABLE + " WHERE " + WH_COL_BRANCH_ID + " = " + branch;

        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                WarehouseObject warehouseObject = new WarehouseObject();
                warehouseObject.setId(c.getInt(c.getColumnIndex(WH_COL_API_ID)));
                warehouseObject.setBranch_id(c.getString(c.getColumnIndex(WH_COL_BRANCH_ID)));
                warehouseObject.setCode(c.getString(c.getColumnIndex(WH_COL_CODE)));
                warehouseObject.setName(c.getString(c.getColumnIndex(WH_COL_NAME)));
                warehouseObject.setDescription(c.getString(c.getColumnIndex(WH_COL_DESCRIPTION)));
                warehouseObject.setEmployee_id(c.getString(c.getColumnIndex(WH_COL_EMPLOYEE_ID)));
                warehouseObject.setArea_post_code_id(c.getString(c.getColumnIndex(WH_COL_AREA_POST_CODE_ID)));
                warehouseObject.setAddress(c.getString(c.getColumnIndex(WH_COL_ADDRESS)));
                warehouseObjectArrayList.add(warehouseObject);
            } while (c.moveToNext());
        }

        c.close();
        return warehouseObjectArrayList;
    }

    public List<SoOpenDetailObject> getSoOpenDetail() {
        List<SoOpenDetailObject> soOpenDetailObjectArrayList = new ArrayList<>();

        String selectQuery = "SELECT " +
                ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_API_ID + "," +
                ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_CODE + "," +
                ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_WAREHOUSE_ID + "," +
                WAREHOUSE_TABLE + "." + WH_COL_EMPLOYEE_ID + "," +
                ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_WAREHOUSE +
                " FROM " + ALL_STOCK_OPNAME_TABLE +
                " LEFT JOIN " + WAREHOUSE_TABLE + " ON " + ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_WAREHOUSE_ID +
                " = " + WAREHOUSE_TABLE + "." + WH_COL_API_ID +
                " WHERE " + ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_CODE + " IN (SELECT " + SO_OPEN_TABLE + "." + SOO_COL_CODE + " from " + SO_OPEN_TABLE + ")";

        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                SoOpenDetailObject soOpenDetailObject = new SoOpenDetailObject();
                soOpenDetailObject.setApi_id(c.getString(c.getColumnIndex(ASO_COL_API_ID)));
                soOpenDetailObject.setCode(c.getString(c.getColumnIndex(ASO_COL_CODE)));
                soOpenDetailObject.setWarehouse_id(c.getString(c.getColumnIndex(ASO_COL_WAREHOUSE_ID)));
                soOpenDetailObject.setEmployee_id(c.getString(c.getColumnIndex(WH_COL_EMPLOYEE_ID)));
                soOpenDetailObject.setWarehouse(c.getString(c.getColumnIndex(ASO_COL_WAREHOUSE)));
                soOpenDetailObjectArrayList.add(soOpenDetailObject);
            } while (c.moveToNext());
        }

        c.close();
        return soOpenDetailObjectArrayList;
    }

    public boolean isItemInserted(String itemNo) {
        boolean isItem = false;
        List<DetailStockOpnameObject> detailStockOpnameObjectList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + DETAIL_STOCK_OPNAME_TABLE + " WHERE "
                + DSO_COL_CODE + " = \"" + itemNo + "\"";
        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                DetailStockOpnameObject detailStockOpnameObject = new DetailStockOpnameObject();
                detailStockOpnameObject.setIdTable(c.getString(c.getColumnIndex(DSO_COL_ID)));
                detailStockOpnameObject.setId(c.getString(c.getColumnIndex(DSO_COL_API_ID)));
                detailStockOpnameObject.setStock_opname_id(c.getString(c.getColumnIndex(DSO_COL_STOCK_OPNAME_ID)));
                detailStockOpnameObject.setProduct_id(c.getString(c.getColumnIndex(DSO_COL_PRODUCT_ID)));
                detailStockOpnameObject.setQty(c.getString(c.getColumnIndex(DSO_COL_QTY)));
                detailStockOpnameObject.setQty_real(c.getString(c.getColumnIndex(DSO_COL_QTY_REAL)));
                detailStockOpnameObject.setDeference(c.getString(c.getColumnIndex(DSO_COL_DIFFERENCE)));
                detailStockOpnameObject.setItem_no(c.getString(c.getColumnIndex(DSO_COL_ITEM_NO)));
                detailStockOpnameObject.setDeleted_at(c.getString(c.getColumnIndex(DSO_COL_DELETED_AT)));
                detailStockOpnameObject.setDeleted_at(c.getString(c.getColumnIndex(DSO_COL_DELETED_AT)));
                detailStockOpnameObject.setName(c.getString(c.getColumnIndex(DSO_COL_NAME)));
                detailStockOpnameObject.setCode(c.getString(c.getColumnIndex(DSO_COL_CODE)));
                detailStockOpnameObject.setWarehouse_id(c.getString(c.getColumnIndex(DSO_COL_WAREHOUSE_ID)));
                detailStockOpnameObject.setBranch_id(c.getString(c.getColumnIndex(DSO_COL_BRANCH_ID)));
                detailStockOpnameObject.setUnit_id(c.getString(c.getColumnIndex(DSO_COL_UNIT_ID)));
                detailStockOpnameObject.setUnit_conversion(c.getString(c.getColumnIndex(DSO_COL_UNIT_CONV)));
                detailStockOpnameObjectList.add(detailStockOpnameObject);
            } while (c.moveToNext());
        }

        if (detailStockOpnameObjectList.size() > 0)
            isItem = true;

        c.close();
        return isItem;
    }

    public String[][] getTableDetailStockOpnameArray(String stock_opname_id, boolean isDetail) {
        ArrayList<DetailStockOpnameObject> detailStockOpnameObjectList = new ArrayList<>();
        String[][] dataList;

        String selectQuery = "SELECT * FROM " + DETAIL_STOCK_OPNAME_TABLE + " WHERE "
                + DSO_COL_STOCK_OPNAME_ID + " = " + stock_opname_id;

        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                DetailStockOpnameObject detailStockOpnameObject = new DetailStockOpnameObject();
                detailStockOpnameObject.setIdTable(c.getString(c.getColumnIndex(DSO_COL_ID)));
                detailStockOpnameObject.setId(c.getString(c.getColumnIndex(DSO_COL_API_ID)));
                detailStockOpnameObject.setStock_opname_id(c.getString(c.getColumnIndex(DSO_COL_STOCK_OPNAME_ID)));
                detailStockOpnameObject.setProduct_id(c.getString(c.getColumnIndex(DSO_COL_PRODUCT_ID)));
                detailStockOpnameObject.setQty(c.getString(c.getColumnIndex(DSO_COL_QTY)));
                detailStockOpnameObject.setQty_real(c.getString(c.getColumnIndex(DSO_COL_QTY_REAL)));
                detailStockOpnameObject.setDeference(c.getString(c.getColumnIndex(DSO_COL_DIFFERENCE)));
                detailStockOpnameObject.setItem_no(c.getString(c.getColumnIndex(DSO_COL_ITEM_NO)));
                detailStockOpnameObject.setDeleted_at(c.getString(c.getColumnIndex(DSO_COL_DELETED_AT)));
                detailStockOpnameObject.setDeleted_at(c.getString(c.getColumnIndex(DSO_COL_DELETED_AT)));
                detailStockOpnameObject.setName(c.getString(c.getColumnIndex(DSO_COL_NAME)));
                detailStockOpnameObject.setCode(c.getString(c.getColumnIndex(DSO_COL_CODE)));
                detailStockOpnameObject.setWarehouse_id(c.getString(c.getColumnIndex(DSO_COL_WAREHOUSE_ID)));
                detailStockOpnameObject.setBranch_id(c.getString(c.getColumnIndex(DSO_COL_BRANCH_ID)));
                detailStockOpnameObject.setUnit_id(c.getString(c.getColumnIndex(DSO_COL_UNIT_ID)));
                detailStockOpnameObject.setUnit_conversion(c.getString(c.getColumnIndex(DSO_COL_UNIT_CONV)));
                detailStockOpnameObject.setUnit_detail(c.getString(c.getColumnIndex(DSO_COL_UNIT_DETAIL)));
                detailStockOpnameObjectList.add(detailStockOpnameObject);
            } while (c.moveToNext());
        }

        c.close();

        if(isDetail) {
            dataList = new String[detailStockOpnameObjectList.size()][6];
            for (int i = 0; i < detailStockOpnameObjectList.size(); i++) {

                DetailStockOpnameObject s = detailStockOpnameObjectList.get(i);

                dataList[i][0] = s.getItem_no();
                dataList[i][1] = s.getName();
                dataList[i][2] = s.getCode();
                dataList[i][3] = s.getQty_real();
                dataList[i][4] = s.getUnit_detail();
                dataList[i][5] = s.getUnit_conversion();
            }
        }else{
            dataList = new String[detailStockOpnameObjectList.size()][4];
            for (int i = 0; i < detailStockOpnameObjectList.size(); i++) {

                DetailStockOpnameObject s = detailStockOpnameObjectList.get(i);

                dataList[i][0] = s.getItem_no();
                dataList[i][1] = s.getCode();
                dataList[i][2] = s.getQty_real();
                dataList[i][3] = s.getUnit_detail();
            }
        }
        return dataList;
    }

    public List<DetailStockOpnameObject> getTableDetailStockOpname(String stock_opname_id) {
        List<DetailStockOpnameObject> detailStockOpnameObjectList = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + DETAIL_STOCK_OPNAME_TABLE + " WHERE "
                + DSO_COL_STOCK_OPNAME_ID + " = " + stock_opname_id;

        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                DetailStockOpnameObject detailStockOpnameObject = new DetailStockOpnameObject();
                detailStockOpnameObject.setIdTable(c.getString(c.getColumnIndex(DSO_COL_ID)));
                detailStockOpnameObject.setId(c.getString(c.getColumnIndex(DSO_COL_API_ID)));
                detailStockOpnameObject.setStock_opname_id(c.getString(c.getColumnIndex(DSO_COL_STOCK_OPNAME_ID)));
                detailStockOpnameObject.setProduct_id(c.getString(c.getColumnIndex(DSO_COL_PRODUCT_ID)));
                detailStockOpnameObject.setQty(c.getString(c.getColumnIndex(DSO_COL_QTY)));
                detailStockOpnameObject.setQty_real(c.getString(c.getColumnIndex(DSO_COL_QTY_REAL)));
                detailStockOpnameObject.setDeference(c.getString(c.getColumnIndex(DSO_COL_DIFFERENCE)));
                detailStockOpnameObject.setItem_no(c.getString(c.getColumnIndex(DSO_COL_ITEM_NO)));
                detailStockOpnameObject.setDeleted_at(c.getString(c.getColumnIndex(DSO_COL_DELETED_AT)));
                detailStockOpnameObject.setDeleted_at(c.getString(c.getColumnIndex(DSO_COL_DELETED_AT)));
                detailStockOpnameObject.setName(c.getString(c.getColumnIndex(DSO_COL_NAME)));
                detailStockOpnameObject.setCode(c.getString(c.getColumnIndex(DSO_COL_CODE)));
                detailStockOpnameObject.setWarehouse_id(c.getString(c.getColumnIndex(DSO_COL_WAREHOUSE_ID)));
                detailStockOpnameObject.setBranch_id(c.getString(c.getColumnIndex(DSO_COL_BRANCH_ID)));
                detailStockOpnameObject.setUnit_id(c.getString(c.getColumnIndex(DSO_COL_UNIT_ID)));
                detailStockOpnameObject.setUnit_conversion(c.getString(c.getColumnIndex(DSO_COL_UNIT_CONV)));
                detailStockOpnameObjectList.add(detailStockOpnameObject);
            } while (c.moveToNext());
        }

        c.close();
        return detailStockOpnameObjectList;
    }

    public List<SubmitStockOpnameObject> getTableJoinStockAndDetail(String id) {
        List<SubmitStockOpnameObject> submitStockOpnameObjectList = new ArrayList<>();

        String selectQuery = "select " + DETAIL_STOCK_OPNAME_TABLE + "." + DSO_COL_STOCK_OPNAME_ID + ", " +
                ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_DATE + ", " +
                ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_BRANCH_ID + ", " +
                ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_WAREHOUSE_ID + ", " +
                ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_UPDATED_BY + ", " +
                DETAIL_STOCK_OPNAME_TABLE + "." + DSO_COL_API_ID + ", " +
                DETAIL_STOCK_OPNAME_TABLE + "." + DSO_COL_EMPLOYEE_ID + ", " +
                DETAIL_STOCK_OPNAME_TABLE + "." + DSO_COL_PRODUCT_ID + ", " +
                DETAIL_STOCK_OPNAME_TABLE + "." + DSO_COL_QTY + ", " +
                DETAIL_STOCK_OPNAME_TABLE + "." + DSO_COL_QTY_REAL + ", " +
                DETAIL_STOCK_OPNAME_TABLE + "." + DSO_COL_ITEM_NO + ", " +
                DETAIL_STOCK_OPNAME_TABLE + "." + DSO_COL_DIFFERENCE + ", " +
                DETAIL_STOCK_OPNAME_TABLE + "." + DSO_COL_UNIT_ID + ", " +
                DETAIL_STOCK_OPNAME_TABLE + "." + DSO_COL_UNIT_CONV +
                " from " + ALL_STOCK_OPNAME_TABLE + " left join " + DETAIL_STOCK_OPNAME_TABLE + " on " +
                ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_API_ID + " = " +
                DETAIL_STOCK_OPNAME_TABLE + "." + DSO_COL_STOCK_OPNAME_ID +
                " where " + ALL_STOCK_OPNAME_TABLE + "." + ASO_COL_API_ID + " = " + id;

        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                SubmitStockOpnameObject submitStockOpnameObject = new SubmitStockOpnameObject();
                submitStockOpnameObject.setAso_api_id(c.getString(c.getColumnIndex(DSO_COL_STOCK_OPNAME_ID)));
                submitStockOpnameObject.setDso_api_id(c.getString(c.getColumnIndex(DSO_COL_API_ID)));
                submitStockOpnameObject.setDate(c.getString(c.getColumnIndex(ASO_COL_DATE)));
                submitStockOpnameObject.setBranch_id(c.getString(c.getColumnIndex(ASO_COL_BRANCH_ID)));
                submitStockOpnameObject.setWarehouse_id(c.getString(c.getColumnIndex(ASO_COL_WAREHOUSE_ID)));
                submitStockOpnameObject.setUpdated_by(c.getString(c.getColumnIndex(ASO_COL_UPDATED_BY)));
                submitStockOpnameObject.setProductid(c.getString(c.getColumnIndex(DSO_COL_PRODUCT_ID)));
                submitStockOpnameObject.setQty(c.getString(c.getColumnIndex(DSO_COL_QTY)));
                submitStockOpnameObject.setQtyreal(c.getString(c.getColumnIndex(DSO_COL_QTY_REAL)));
                submitStockOpnameObject.setItem_no(c.getString(c.getColumnIndex(DSO_COL_ITEM_NO)));
                submitStockOpnameObject.setUnit_id(c.getString(c.getColumnIndex(DSO_COL_UNIT_ID)));
                submitStockOpnameObject.setUnit_conv(c.getString(c.getColumnIndex(DSO_COL_UNIT_CONV)));
                submitStockOpnameObject.setEmployee_id(c.getString(c.getColumnIndex(DSO_COL_EMPLOYEE_ID)));
                submitStockOpnameObjectList.add(submitStockOpnameObject);
            } while (c.moveToNext());
        }

        c.close();

        return submitStockOpnameObjectList;
    }

    public Integer getMaxIdASO() {
        int maxId = 0;

        String selectQuery = "SELECT " + ASO_COL_API_ID + " FROM " + ALL_STOCK_OPNAME_TABLE + " ORDER BY " + ASO_COL_API_ID + " DESC LIMIT 1;";

        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                maxId = c.getInt(c.getColumnIndex(ASO_COL_API_ID));
            } while (c.moveToNext());
        }

        c.close();
        return maxId;
    }

    public Integer getMaxIdDSO() {
        int maxId = 0;

        String selectQuery = "SELECT " + DSO_COL_API_ID + " FROM " + DETAIL_STOCK_OPNAME_TABLE + " ORDER BY " + DSO_COL_API_ID + " DESC LIMIT 1;";

        Timber.i(selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                maxId = c.getInt(c.getColumnIndex(DSO_COL_API_ID));
            } while (c.moveToNext());
        }

        c.close();
        return maxId;
    }
}
