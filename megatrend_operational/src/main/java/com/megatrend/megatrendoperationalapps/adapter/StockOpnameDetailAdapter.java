package com.megatrend.megatrendoperationalapps.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.megatrend.megatrendoperationalapps.R;
import com.megatrend.megatrendoperationalapps.object.DetailStockOpnameObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 1/11/2018.
 */

public class StockOpnameDetailAdapter extends RecyclerView.Adapter<StockOpnameDetailAdapter.StockOpnameDetailHolder> {

    List<DetailStockOpnameObject> detailStockOpnameObjects;
    Context mContext;

    public StockOpnameDetailAdapter(Context context, List<DetailStockOpnameObject> detailStockOpnameObjectList) {
        this.mContext = context;
        this.detailStockOpnameObjects = detailStockOpnameObjectList;
    }

    @Override
    public StockOpnameDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_stock_opname_detail, parent, false);
        return new StockOpnameDetailHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StockOpnameDetailHolder holder, int position) {
        holder.tvItemNo.setText(detailStockOpnameObjects.get(position).getItem_no());
        holder.tvProductCode.setText(detailStockOpnameObjects.get(position).getCode());
        holder.tvProductName.setText(detailStockOpnameObjects.get(position).getName());
        //holder.tvQtyDiff.setText(detailStockOpnameObjects.get(position).getDeference());
        holder.tvRealStock.setText(detailStockOpnameObjects.get(position).getQty_real());
    }

    @Override
    public int getItemCount() {
        return detailStockOpnameObjects.size();
    }

    public class StockOpnameDetailHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvItemNo)
        TextView tvItemNo;
        @BindView(R.id.tvProductCode)
        TextView tvProductCode;
        @BindView(R.id.tvProductName)
        TextView tvProductName;
        @BindView(R.id.tvRealStock)
        TextView tvRealStock;
        @BindView(R.id.tvQtyDiff)
        TextView tvQtyDiff;
        @BindView(R.id.card_view)
        CardView cardView;

        public StockOpnameDetailHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
