package com.megatrend.megatrendoperationalapps.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.megatrend.megatrendoperationalapps.R;
import com.megatrend.megatrendoperationalapps.object.SoOpenDetailObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SoOpenDetailAdapter extends RecyclerView.Adapter<SoOpenDetailAdapter.SoOpenDetailHolder> {
    Context context;
    List<SoOpenDetailObject> soOpenDetailObjectList;

    public SoOpenDetailAdapter(Context context, List<SoOpenDetailObject> soOpenDetailObjectList) {
        this.context = context;
        this.soOpenDetailObjectList = soOpenDetailObjectList;
    }

    @NonNull
    @Override
    public SoOpenDetailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_name_warehouse, parent, false);
        return new SoOpenDetailHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SoOpenDetailHolder holder, int position) {
        final SoOpenDetailObject soOpenDetailObject = soOpenDetailObjectList.get(position);
        holder.tvName.setText(soOpenDetailObject.getWarehouse() + "(" + soOpenDetailObject.getCode() + ")");
    }

    @Override
    public int getItemCount() {
        return soOpenDetailObjectList.size();
    }

    public class SoOpenDetailHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        //        @BindView(R.id.tvAddress)
//        TextView tvAddress;
        @BindView(R.id.card_view)
        CardView cardView;

        public SoOpenDetailHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
