package com.megatrend.megatrendoperationalapps.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.megatrend.megatrendoperationalapps.R;
import com.megatrend.megatrendoperationalapps.object.WarehouseObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by megatrend on 1/3/2018.
 */

public class AllWarehouseAdapter extends RecyclerView.Adapter<AllWarehouseAdapter.AllWHHolder> {
    List<WarehouseObject> warehouseObjects;
    Context mContext;


    public AllWarehouseAdapter(Context context, List<WarehouseObject> warehouseObjectList) {
        this.mContext = context;
        warehouseObjects = warehouseObjectList;
    }

    @Override
    public AllWHHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_name_warehouse, parent, false);
        return new AllWHHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AllWHHolder holder, int position) {
        final WarehouseObject warehouseObject = warehouseObjects.get(position);
        holder.tvName.setText(warehouseObject.getName());
//        holder.tvAddress.setText(warehouseObject.getAddress());
    }

    @Override
    public int getItemCount() {
        return warehouseObjects.size();
    }

    public class AllWHHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        //        @BindView(R.id.tvAddress)
//        TextView tvAddress;
        @BindView(R.id.card_view)
        CardView cardView;

        public AllWHHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
