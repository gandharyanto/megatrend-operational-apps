package com.megatrend.megatrendoperationalapps.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.megatrend.megatrendoperationalapps.R;
import com.megatrend.megatrendoperationalapps.object.StockOpnameDataObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 1/6/2018.
 */

public class AllStockOpnameAdapter extends RecyclerView.Adapter<AllStockOpnameAdapter.AllStockOpnameHolder> {

    List<StockOpnameDataObject> stockOpnameDataObjects;
    Context mContext;

    public AllStockOpnameAdapter(Context context, List<StockOpnameDataObject> stockOpnameDataObjectList) {
        this.mContext = context;
        this.stockOpnameDataObjects = stockOpnameDataObjectList;
    }

    @Override
    public AllStockOpnameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_all_stock_opname, parent, false);
        return new AllStockOpnameHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AllStockOpnameHolder holder, int position) {
        holder.tvWarehouse.setText(stockOpnameDataObjects.get(position).getWarehouse());
        holder.tvDate.setText(stockOpnameDataObjects.get(position).getDate());
        holder.tvAdmin.setText(stockOpnameDataObjects.get(position).getUpdated_by());
    }

    @Override
    public int getItemCount() {
        return stockOpnameDataObjects.size();
    }

    public void clear() {
        int size = this.stockOpnameDataObjects.size();
        this.stockOpnameDataObjects.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void update(List<StockOpnameDataObject> stockOpnameDataObjectList) {
        stockOpnameDataObjects.clear();
        stockOpnameDataObjects.addAll(stockOpnameDataObjectList);
        notifyDataSetChanged();
    }

    public class AllStockOpnameHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvWarehouse)
        TextView tvWarehouse;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvAdmin)
        TextView tvAdmin;
        @BindView(R.id.card_view)
        CardView cardView;

        public AllStockOpnameHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
