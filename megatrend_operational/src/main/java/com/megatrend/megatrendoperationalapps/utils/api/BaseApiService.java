package com.megatrend.megatrendoperationalapps.utils.api;

import android.support.annotation.NonNull;

import com.megatrend.megatrendoperationalapps.model.ResponseGetBranch;
import com.megatrend.megatrendoperationalapps.model.ResponseGetDetailStockOpname;
import com.megatrend.megatrendoperationalapps.model.ResponseGetStockOpnameData;
import com.megatrend.megatrendoperationalapps.model.ResponseGetUnit;
import com.megatrend.megatrendoperationalapps.model.ResponseGetWarehouse;
import com.megatrend.megatrendoperationalapps.model.ResponsePostAddDataStockOpname;
import com.megatrend.megatrendoperationalapps.model.ResponsePostChangeStatus;
import com.megatrend.megatrendoperationalapps.model.ResponsePostCreateSTO;
import com.megatrend.megatrendoperationalapps.model.ResponsePostDataProductByItem;
import com.megatrend.megatrendoperationalapps.model.ResponsePostGetSoOpen;
import com.megatrend.megatrendoperationalapps.model.ResponsePostLoginUser;
import com.megatrend.megatrendoperationalapps.model.ResponsePostProductbyItem;
import com.megatrend.megatrendoperationalapps.model.ResponsePostUpdateDataStockOpname;
import com.megatrend.megatrendoperationalapps.object.AddStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.BranchObject;
import com.megatrend.megatrendoperationalapps.object.DetailStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.JsonLogin;
import com.megatrend.megatrendoperationalapps.object.ProductObject;
import com.megatrend.megatrendoperationalapps.object.StockOpnameDataObject;
import com.megatrend.megatrendoperationalapps.object.UnitObject;
import com.megatrend.megatrendoperationalapps.object.WarehouseObject;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by megatrend on 1/3/2018.
 */

public interface BaseApiService {

    // getBranch
//    @GET("/development/public/api/branch/getBranch") //dev
//    @GET("/backend/public/index.php/api/branch/getBranch")
    @GET("/backend/public/api/branch/getBranch")
    //pro
    Call<ResponseGetBranch> getBranch();

    // getWerehouse
//    @GET("/development/public/api/werehouse/getAllData")//dev
//    @GET("/backend/public/index.php/api/werehouse/getAllData")
    @GET("/backend/public/api/werehouse/getAllData")
//pro
    Call<ResponseGetWarehouse> getAllData(@QueryMap Map<String, String> options);

    // getStockOpnameData
//    @GET("/development/public/api/stockopname/getData")//dev
//    @GET("/backend/public/index.php/api/stockopname/getData")
    @GET("/backend/public/api/stockopname/getData")
//pro
    Call<ResponseGetStockOpnameData> getStockOpnameData();

    //getProductbyItem
//    @POST("/development/public/api/stockopname/getdataProductbyItem")//dev
//    @POST("/backend/public/index.php/api/stockopname/getdataProductbyItem")
    @POST("/backend/public/api/stockopname/getdataProductbyItem")
//pro
    Call<ResponsePostProductbyItem> getdataProductbyItem(@QueryMap Map<String, String> options);

    @POST("/backend/public/api/stockopname/getdataProductbyItem")
//pro
    Call<ResponsePostDataProductByItem> getProductbyItem(@QueryMap Map<String, String> options);

    /*data.put("productid[]", submitStockOpnameObjectList.get(i).getProductid());
                    data.put("qty[]", submitStockOpnameObjectList.get(i).getQty());
                    data.put("qtyreal[]", submitStockOpnameObjectList.get(i).getQtyreal());
                    data.put("item_no[]", submitStockOpnameObjectList.get(i).getItem_no());
                    data.put("selisih[]", submitStockOpnameObjectList.get(i).getSelisih());*/
    //addDataStockOpname
//    @POST("/development/public/api/stockopname/addData")//dev
//    @POST("/backend/public/index.php/api/stockopname/addData")
    @POST("/backend/public/api/stockopname/addData")
//pro
    Call<ResponsePostAddDataStockOpname> postAddDataStockOpname(@QueryMap Map<String, String> options,
                                                                @Query("productid") String productid,
                                                                @Query("qty") String qty,
                                                                @Query("unit_conversion") String unit_conversion,
                                                                @Query("item_no") String item_no,
                                                                @Query("unit_id") String unit_id);

    //addDataStockOpname
//    @POST("/development/public/api/stockopname/addData")//dev
//    @POST("/backend/public/index.php/api/stockopname/addData")
    @POST("/backend/public/api/stockopname/addData")
//pro
    Call<ResponsePostAddDataStockOpname> postAddDataStockOpname2(@QueryMap Map<String, String> options,
                                                                 @Query("employee_id") String employee_id,
                                                                 @Query("productid") String productid,
                                                                 @Query("qty") String qty,
                                                                 @Query("unit_conversion") String unit_conversion,
                                                                 @Query("unit_id") String unit_id);

    //getStockOpnameDetail
//    @GET("/development/public/api/stockopname/getDetail/{id_stockopname}")//dev
//    @GET("/backend/public/index.php/api/stockopname/getDetail/{id_stockopname}")
    @GET("/backend/public/api/stockopname/getDetail/{id_stockopname}")
//pro
    Call<ResponseGetDetailStockOpname> getDetailStockOpname(@Path(value = "id_stockopname") String id_stockopname);

    //getUnitCon
//    @GET("/development/public/api/unit/getUnitCon/{item_id}")//dev
//    @GET("/backend/public/index.php/api/unit/getUnitCon/{item_id}")
    @GET("/backend/public/api/unit/getUnitCon/{item_id}")
//pro
    Call<ResponseGetUnit> getUnit(@Path(value = "item_id") String item_id);

    //postUpdateData
//    @POST("/development/public/api/stockopname/updateData/{id_stockOpname}")//dev
//    @POST("/backend/public/index.php/api/stockopname/updateData/{id_stockOpname}")
    @POST("/backend/public/api/stockopname/updateData/{id_stockOpname}")
//pro
    Call<ResponsePostUpdateDataStockOpname> postUpdateDataStockOpname(@Path(value = "id_stockOpname") String id_stockopname, @QueryMap Map<String, String> options);

    @Headers("Content-Type: application/json")
//    @POST("/development/public/api/android/userlogin")//dev
//    @POST("/backend/public/index.php/api/android/userlogin")
    @POST("/backend/public/api/android/userlogin")
//pro
    Call<ResponsePostLoginUser> postLoginUser(@Body JsonLogin jsonData);

    //    @POST("/development/public/api/stockopname/createHeader")//dev
    @POST("/backend/public/index.php/api/stockopname/createHeader")
//pro
    Call<ResponsePostCreateSTO> postCreateSTO(@QueryMap Map<String, String> options);

    //    @POST("/development/public/api/stockopname/createHeader")//dev
//    @POST("/backend/public/index.php/api/stockopname/createHeader")//pro
    @POST("/backend/public/api/stockopname/getSoOpen")
//pro
    Call<ResponsePostGetSoOpen> postGetSoOpen(@QueryMap Map<String, String> options);

    @POST("/backend/public/api/stockopname/changeStatus")
//pro
    Call<ResponsePostChangeStatus> postChangeStatus(@QueryMap Map<String, String> options);

    interface GetDataProductDetailCallback {
        void onSuccess(@NonNull List<AddStockOpnameObject> value);

        void onError(@NonNull Throwable throwable);
    }

    interface GetProductDetailCallback {
        void onSuccess(@NonNull List<ProductObject> value);

        void onError(@NonNull Throwable throwable);
    }

    interface GetStockOpnameCallback {
        void onSuccess(@NonNull List<StockOpnameDataObject> value);

        void onError(@NonNull Throwable throwable);
    }

    interface GetBranchCallback {
        void onSuccess(@NonNull List<BranchObject> value);

        void onError(@NonNull Throwable throwable);
    }

    interface GetWarehouseCallback {
        void onSuccess(@NonNull List<WarehouseObject> value);

        void onError(@NonNull Throwable throwable);
    }

    interface PostAddStockOpnameCallback {
        void onSuccess(@NonNull String value);

        void onError(@NonNull Throwable throwable);
    }

    interface PostLoginUserCallback {
        void onSuccess(@NonNull ResponsePostLoginUser value);

        void onError(@NonNull Throwable throwable);
    }

    interface PostCreateSTOCallback {
        void onSuccess(@NonNull ResponsePostCreateSTO value);

        void onError(@NonNull Throwable throwable);
    }

    interface PostGetSOOpenCallback {
        void onSuccess(@NonNull ResponsePostGetSoOpen value);

        void onError(@NonNull Throwable throwable);
    }

    interface PostChangeStatusCallback {
        void onSuccess(@NonNull ResponsePostChangeStatus value);

        void onError(@NonNull Throwable throwable);
    }

    interface GetDetailStockOpnameCallback {
        void onSuccess(@NonNull List<DetailStockOpnameObject> value);

        void onError(@NonNull Throwable throwable);
    }

    interface GetUnitCallback {
        void onSuccess(@NonNull List<UnitObject> value);

        void onError(@NonNull Throwable throwable);
    }

    interface PostUpdateStockOpnameCallback {
        void onSuccess(@NonNull String value);

        void onError(@NonNull Throwable throwable);
    }


    public interface Listener {
        void onClickButtonNext();
    }

}
