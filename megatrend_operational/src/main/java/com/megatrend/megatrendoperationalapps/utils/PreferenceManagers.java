package com.megatrend.megatrendoperationalapps.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Gandha on 23/07/2017.
 */

public class PreferenceManagers {

    public static void setData(String key, String data, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putString(key, data);
        prefEditor.apply();
    }

    public static String getData(String key, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String data = prefs.getString(key, null);
        return data;
    }

    public static void clearDataWithKey(String key, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        if (prefs.contains(key)) {
            editor.remove(key);
            editor.apply();
        }
    }

    public static void setDataWithSameKey(String key, String data, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        if (prefs.contains(key)) {
            editor.remove(key);
            editor.apply();
        }
        editor.putString(key, data);
        editor.commit();
    }

    public static void clear(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();
    }

    public static boolean hasData(String key, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(key, null) != null;
    }
}
