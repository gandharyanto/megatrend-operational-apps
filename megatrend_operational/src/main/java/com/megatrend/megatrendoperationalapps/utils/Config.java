package com.megatrend.megatrendoperationalapps.utils;

import android.app.Activity;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by megatrend on 1/3/2018.
 */

public class Config {
    public static final String KEY_BRANCH_JSON = "keyBranchJson";
    public static final String KEY_WEREHOUSE_JSON = "keyWerehouseJson";
    public static final String KEY_DETAIL_STOCK_OPNAME_JSON = "keyDetailStockOpnameJson";
    public static final String KEY_ALL_STOCK_OPNAME_JSON = "keyAllStockOpnameJson";
    public static final String KEY_BRANCH_INPUT = "key_branch_input";
    public static final String KEY_WAREHOUSE_INPUT = "key_warehouse_input";
    public static final String KEY_DETAIL_SO_INPUT = "key_detail_so_input";
    public static final String KEY_ADD = "key_add";
    public static final String KEY_UPDATE = "key_update";
    public static final String KEY_SUBMIT = "key_submit";
    public static final String KEY_USERNAME = "key_username";
    public static final String KEY_USERMAIL = "key_usermail";
    public static final String KEY_ID_SALESMAN = "key_id_salesman";
    public static final String KEY_ID_BRANCH = "key_id_branch";
    public static final String KEY_ID_WAREHOUSE = "key_id_warehouse";
    public static final String KEY_WAREHOUSE = "key_warehouse";
    public static final String KEY_ID_EMPLOYEE = "key_id_employee";
    public static final String KEY_CODE_ASO = "key_code_aso";
    public static final String KEY_ID_API_ASO = "key_id_api_aso";
    public static final String KEY_INPUT_MANUAL = "key_input_manual";
    public static final String KEY_LAST_LOGIN = "key_last_login";
    public static final String KEY_ADD_ONTOUCH = "key_add_ontouch";
    public static final String KEY_CODE = "key_code";

    @SuppressWarnings("deprecation")
    public static int getWidth(Activity context) {
        int Measuredwidth = 0;
        Point size = new Point();
        WindowManager w = context.getWindowManager();

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.HONEYCOMB_MR2) {
            w.getDefaultDisplay().getSize(size);

            Measuredwidth = size.x;
        } else {
            Display d = w.getDefaultDisplay();
            Measuredwidth = d.getWidth();
        }

        return Measuredwidth;
    }
}
