package com.megatrend.megatrendoperationalapps.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import com.google.zxing.integration.android.IntentIntegrator;
import com.megatrend.megatrendoperationalapps.activities.BarcodeScanActivity;

import timber.log.Timber;

public class BarcodeScanUtils {

    public static void scan(Activity activity) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
        integrator.setPrompt("Scan a barcode");
        integrator.setCameraId(Camera.CameraInfo.CAMERA_FACING_BACK);  // Use a specific camera of the device
        integrator.setBeepEnabled(true);
        integrator.setBarcodeImageEnabled(false);
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(BarcodeScanActivity.class);
        integrator.initiateScan();
    }

    public static class CheckPermission implements ActivityCompat.OnRequestPermissionsResultCallback {

        private Activity act;
        private boolean isCheckPermissionHit = false;

        private boolean hasPermissions(Context context, String... permissions) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
                for (String permission : permissions) {
                    if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                        return false;
                    }
                }
            }
            return true;
        }

        public void CheckPermission(Activity act) {
            this.act = act;
            if (!isCheckPermissionHit) {
                isCheckPermissionHit = true;
                String[] PERMISSIONS = {Manifest.permission.CAMERA};
                if (!hasPermissions(act, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(act, PERMISSIONS, 1);
                } else {
                    BarcodeScanUtils.scan(act);
                }
            } else {
                BarcodeScanUtils.scan(act);
            }
        }

        public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
            switch (requestCode) {
                case 1: {
                    boolean isGranted = true;
                    for (int grant : grantResults) {
                        if (grant != PackageManager.PERMISSION_GRANTED) {
                            Timber.d("Mohon berikan akses ke semua permintaan");
                            isGranted = false;
                            act.finish();
                        }
                    }

                    if (isGranted)
                        BarcodeScanUtils.scan(act);
                }
            }
        }
    }
}
