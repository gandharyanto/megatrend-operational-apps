package com.megatrend.megatrendoperationalapps.utils.api;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import com.megatrend.megatrendoperationalapps.model.ResponseGetBranch;
import com.megatrend.megatrendoperationalapps.model.ResponseGetDetailStockOpname;
import com.megatrend.megatrendoperationalapps.model.ResponseGetStockOpnameData;
import com.megatrend.megatrendoperationalapps.model.ResponseGetUnit;
import com.megatrend.megatrendoperationalapps.model.ResponseGetWarehouse;
import com.megatrend.megatrendoperationalapps.model.ResponsePostAddDataStockOpname;
import com.megatrend.megatrendoperationalapps.model.ResponsePostChangeStatus;
import com.megatrend.megatrendoperationalapps.model.ResponsePostCreateSTO;
import com.megatrend.megatrendoperationalapps.model.ResponsePostDataProductByItem;
import com.megatrend.megatrendoperationalapps.model.ResponsePostGetSoOpen;
import com.megatrend.megatrendoperationalapps.model.ResponsePostLoginUser;
import com.megatrend.megatrendoperationalapps.model.ResponsePostProductbyItem;
import com.megatrend.megatrendoperationalapps.model.ResponsePostUpdateDataStockOpname;
import com.megatrend.megatrendoperationalapps.object.AddStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.BranchObject;
import com.megatrend.megatrendoperationalapps.object.DetailStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.JsonLogin;
import com.megatrend.megatrendoperationalapps.object.StockOpnameDataObject;
import com.megatrend.megatrendoperationalapps.object.WarehouseObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by megatrend on 1/3/2018.
 */

public class ApiUtils {

    //Mendeklarasi API
    private static final String BASE_URL_API = "http://103.200.7.184/"; //pro
    //    private static final String BASE_URL_API = "http://103.200.4.92/"; //dev
    static ResponsePostAddDataStockOpname responsePostAddDataStockOpname = new ResponsePostAddDataStockOpname();
    private static BaseApiService mApiService;
    private static List<BranchObject> branchObjectList = new ArrayList<>();
    private static List<WarehouseObject> warehouseObjectList = new ArrayList<>();
    private static List<DetailStockOpnameObject> detailStockOpnameObjectList = new ArrayList<>();
    private static List<StockOpnameDataObject> stockOpnameDataObjectList = new ArrayList<>();
    private static List<AddStockOpnameObject> addStockOpnameObject = new ArrayList<>();

    public static BaseApiService getAPIService() {
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }

    public static boolean isURLReachable(Context context, String uRL) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.
                        CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            try {
                URL url = new URL(uRL);   // Change to "http://google.com" for www test.

                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                urlc.setConnectTimeout(5 * 1000);          // 5 s.

                urlc.connect();
                if (urlc.getResponseCode() == 200) { // 200 = "OK" code  (http connection is fine).
                    Timber.i("Success !");
                    return true;
                } else {
                    return false;
                }
            } catch (IOException e) {
                return false;
            }
        }
        return false;
    }

    public static void getBranchAPI(Activity act, final BaseApiService.GetBranchCallback getBranchCallback) {
        //progressDialog = ProgressDialog.show(act, null, "Get Branch ...", true, false);
        mApiService = ApiUtils.getAPIService();
        mApiService.getBranch().enqueue(new Callback<ResponseGetBranch>() {
            @Override
            public void onResponse(@NonNull Call<ResponseGetBranch> call, @NonNull Response<ResponseGetBranch> response) {
                if (response.isSuccessful()) {
                    //progressDialog.dismiss();

                    branchObjectList = response.body().getData();
                    getBranchCallback.onSuccess(branchObjectList);
                } else {
                    //progressDialog.dismiss();
                    branchObjectList = null;
                    getBranchCallback.onSuccess(branchObjectList);
                    Timber.e("Gagal mengambil data branch - 1 (" + response.message() + ")");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseGetBranch> call, @NonNull Throwable t) {
                //progressDialog.dismiss();
                branchObjectList = null;
                getBranchCallback.onError(t);
                Timber.e("Gagal mengambil data branch - 2 (" + t.getMessage() + ")");
            }
        });
    }

    public static void getWarehouseAPI(Activity act, int branch_id, final BaseApiService.GetWarehouseCallback getWarehouseCallback) {
        //progressDialog = ProgressDialog.show(this, null, "Get Branch Detail ...", true, false);

        Map<String, String> data = new HashMap<>();
        data.put("branch_id", String.valueOf(branch_id));

        mApiService = ApiUtils.getAPIService();
        mApiService.getAllData(data).enqueue(new Callback<ResponseGetWarehouse>() {
            @Override
            public void onResponse(@NonNull Call<ResponseGetWarehouse> call, @NonNull Response<ResponseGetWarehouse> response) {
                if (response.isSuccessful()) {
                    //progressDialog.dismiss();

                    warehouseObjectList = response.body().getData();
                    getWarehouseCallback.onSuccess(warehouseObjectList);
                } else {
                    //progressDialog.dismiss();
                    warehouseObjectList = null;
                    getWarehouseCallback.onSuccess(warehouseObjectList);
                    Timber.e("Gagal mengambil data werehouse - 1 (" + response.message() + ")");
                }
            }

            @Override
            public void onFailure(Call<ResponseGetWarehouse> call, Throwable t) {
                //progressDialog.dismiss();
                warehouseObjectList = null;
                getWarehouseCallback.onError(t);
                Timber.e("Gagal mengambil data warehouuse - 2 (" + t.getMessage() + ")");
            }
        });
    }

    public static void getDetailStockOpname(Activity act, String id, final BaseApiService.GetDetailStockOpnameCallback getDetailStockOpnameCallback) {

        mApiService = ApiUtils.getAPIService();
        mApiService.getDetailStockOpname(id).enqueue(new Callback<ResponseGetDetailStockOpname>() {
            @Override
            public void onResponse(Call<ResponseGetDetailStockOpname> call, Response<ResponseGetDetailStockOpname> response) {
                if (response.isSuccessful()) {
                    detailStockOpnameObjectList = response.body().getMsg();
                } else {
                    detailStockOpnameObjectList = null;
                }
                getDetailStockOpnameCallback.onSuccess(detailStockOpnameObjectList);
            }

            @Override
            public void onFailure(Call<ResponseGetDetailStockOpname> call, Throwable t) {
                getDetailStockOpnameCallback.onError(t);
                Timber.e("Gagal mengambil data detail stock opname - 2 (" + t.getMessage() + ")");
            }
        });

    }

    public static void getStockOpname(Activity act, final BaseApiService.GetStockOpnameCallback getStockOpnameCallback) {

        mApiService = ApiUtils.getAPIService();
        mApiService.getStockOpnameData().enqueue(new Callback<ResponseGetStockOpnameData>() {
            @Override
            public void onResponse(@NonNull Call<ResponseGetStockOpnameData> call, @NonNull Response<ResponseGetStockOpnameData> response) {
                if (response.isSuccessful()) {
                    stockOpnameDataObjectList = response.body().getData();
                } else {
                    stockOpnameDataObjectList = null;
                }
                getStockOpnameCallback.onSuccess(stockOpnameDataObjectList);
            }

            @Override
            public void onFailure(@NonNull Call<ResponseGetStockOpnameData> call, @NonNull Throwable t) {
                getStockOpnameCallback.onError(t);
                Timber.e("Gagal mengambil data detail stock opname - 2 (" + t.getMessage() + ")");
            }
        });

    }

    public static void getdataProductbyItem(Activity act, String branch_id, String warehouse_id, String item_no, final BaseApiService.GetDataProductDetailCallback getDataProductDetailCallback) {
        Map<String, String> data = new HashMap<>();
        data.put("branch_id", branch_id);
        data.put("warehouse_id", warehouse_id);
        data.put("item_no", item_no);

        mApiService = ApiUtils.getAPIService();
        mApiService.getdataProductbyItem(data).enqueue(new Callback<ResponsePostProductbyItem>() {
            @Override
            public void onResponse(@NonNull Call<ResponsePostProductbyItem> call, @NonNull Response<ResponsePostProductbyItem> response) {

                if (response.isSuccessful()) {
                    //progressDialog.dismiss();

                    addStockOpnameObject = response.body().getData();
                    getDataProductDetailCallback.onSuccess(addStockOpnameObject);
                } else {
                    //progressDialog.dismiss();
                    addStockOpnameObject = null;
                    getDataProductDetailCallback.onSuccess(addStockOpnameObject);
                    Timber.e("Gagal mengambil data werehouse - 1 (" + response.message() + ")");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponsePostProductbyItem> call, @NonNull Throwable t) {

                addStockOpnameObject = null;
                if (getDataProductDetailCallback != null)
                    getDataProductDetailCallback.onError(t);
            }
        });
    }

    public static void getProductbyItem(Activity act, String branch_id, String warehouse_id, String item_no, final BaseApiService.GetProductDetailCallback getProductDetailCallback) {
        Map<String, String> data = new HashMap<>();
        data.put("branch_id", branch_id);
        data.put("warehouse_id", warehouse_id);
        data.put("item_no", item_no);

        mApiService = ApiUtils.getAPIService();
        mApiService.getProductbyItem(data).enqueue(new Callback<ResponsePostDataProductByItem>() {
            @Override
            public void onResponse(@NonNull Call<ResponsePostDataProductByItem> call, @NonNull Response<ResponsePostDataProductByItem> response) {

                if (response.isSuccessful()) {
                    //progressDialog.dismiss();
                    getProductDetailCallback.onSuccess(response.body().getData());
                } else {
                    //progressDialog.dismiss();
                    addStockOpnameObject = null;
                    getProductDetailCallback.onSuccess(response.body().getData());
                    Timber.e("Gagal mengambil data werehouse - 1 (" + response.message() + ")");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponsePostDataProductByItem> call, @NonNull Throwable t) {

                addStockOpnameObject = null;
                if (getProductDetailCallback != null)
                    getProductDetailCallback.onError(t);
            }
        });
    }

    public static void PostAddStockOpname(Activity act, Map<String, String> data, String productid, String qty, String unitConv, String item_no, String unitId, final BaseApiService.PostAddStockOpnameCallback postAddStockOpnameCallback) {
        mApiService = ApiUtils.getAPIService();
        /*postAddDataStockOpname(@QueryMap Map<String, String> options,
                                                                @Query("productid") String productid,
                                                                @Query("qty") String qty,
                                                                @Query("unit_conversion") String unit_conversion,
                                                                @Query("item_no") String item_no,
                                                                @Query("unit_id") String unit_id);*/
        mApiService.postAddDataStockOpname(data, productid, qty, unitConv, item_no, unitId).enqueue(new Callback<ResponsePostAddDataStockOpname>() {
            @Override
            public void onResponse(Call<ResponsePostAddDataStockOpname> call, Response<ResponsePostAddDataStockOpname> response) {
                if (response.isSuccessful()) {
                    String status = response.body().getStatus();
                    String msg = response.body().getMsg();
                    String title = response.body().getTitle();
                    postAddStockOpnameCallback.onSuccess(title);
                }
            }

            @Override
            public void onFailure(Call<ResponsePostAddDataStockOpname> call, Throwable t) {
                postAddStockOpnameCallback.onError(t);
            }
        });
    }

    public static void PostAddStockOpname2(Activity act, Map<String, String> data, String employee_id, String productid, String qty, String unitConv, String unitId, final BaseApiService.PostAddStockOpnameCallback postAddStockOpnameCallback) {
        mApiService = ApiUtils.getAPIService();
        /*postAddDataStockOpname(@Query("employee_id") String employee_id,
                                                                @Query("productid") String productid,
                                                                @Query("qty") String qty,
                                                                @Query("unit_conversion") String unit_conversion,
                                                                @Query("unit_id") String unit_id);*/
        mApiService.postAddDataStockOpname2(data, employee_id, productid, qty, unitConv, unitId).enqueue(new Callback<ResponsePostAddDataStockOpname>() {
            @Override
            public void onResponse(Call<ResponsePostAddDataStockOpname> call, Response<ResponsePostAddDataStockOpname> response) {
                if (response.isSuccessful()) {
                    String status = response.body().getStatus();
                    String msg = response.body().getMsg();
                    String title = response.body().getTitle();
                    postAddStockOpnameCallback.onSuccess(title);
                }
            }

            @Override
            public void onFailure(Call<ResponsePostAddDataStockOpname> call, Throwable t) {
                postAddStockOpnameCallback.onError(t);
            }
        });
    }

    public static void PostLoginUser(Activity act, JsonLogin jsonPost, final BaseApiService.PostLoginUserCallback postLoginUserCallback) {
        mApiService = ApiUtils.getAPIService();
        mApiService.postLoginUser(jsonPost).enqueue(new Callback<ResponsePostLoginUser>() {
            @Override
            public void onResponse(Call<ResponsePostLoginUser> call, Response<ResponsePostLoginUser> response) {
                if (response.isSuccessful()) {
                    postLoginUserCallback.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsePostLoginUser> call, Throwable t) {
                postLoginUserCallback.onError(t);
            }
        });
    }

    public static void PostCreateSTO(Activity act, Map<String, String> data, final BaseApiService.PostCreateSTOCallback postCreateSTOCallback) {
        mApiService = ApiUtils.getAPIService();
        mApiService.postCreateSTO(data).enqueue(new Callback<ResponsePostCreateSTO>() {
            @Override
            public void onResponse(Call<ResponsePostCreateSTO> call, Response<ResponsePostCreateSTO> response) {
                if (response.isSuccessful()) {
                    postCreateSTOCallback.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsePostCreateSTO> call, Throwable t) {
                postCreateSTOCallback.onError(t);
            }
        });
    }

    public static void PostGetSOOpen(Activity act, Map<String, String> data, final BaseApiService.PostGetSOOpenCallback postGetSOOpenCallback) {
        mApiService = ApiUtils.getAPIService();
        mApiService.postGetSoOpen(data).enqueue(new Callback<ResponsePostGetSoOpen>() {
            @Override
            public void onResponse(Call<ResponsePostGetSoOpen> call, Response<ResponsePostGetSoOpen> response) {
                if (response.isSuccessful()) {
                    postGetSOOpenCallback.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsePostGetSoOpen> call, Throwable t) {
                postGetSOOpenCallback.onError(t);
            }
        });
    }

    public static void postChangeStatus(Activity act, Map<String, String> data, final BaseApiService.PostChangeStatusCallback postChangeStatusCallback) {
        mApiService = ApiUtils.getAPIService();
        mApiService.postChangeStatus(data).enqueue(new Callback<ResponsePostChangeStatus>() {
            @Override
            public void onResponse(Call<ResponsePostChangeStatus> call, Response<ResponsePostChangeStatus> response) {
                if (response.isSuccessful()) {
                    postChangeStatusCallback.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsePostChangeStatus> call, Throwable t) {
                postChangeStatusCallback.onError(t);
            }
        });
    }

    public static void PostUpdateStockOpname(Activity act, String id_stock, Map<String, String> data, final BaseApiService.PostUpdateStockOpnameCallback postUpdateStockOpnameCallback) {
        mApiService = ApiUtils.getAPIService();
        mApiService.postUpdateDataStockOpname(id_stock, data).enqueue(new Callback<ResponsePostUpdateDataStockOpname>() {
            @Override
            public void onResponse(Call<ResponsePostUpdateDataStockOpname> call, Response<ResponsePostUpdateDataStockOpname> response) {
                if (response.isSuccessful()) {
                    String status = response.body().getStatus();
                    String msg = response.body().getMsg();
                    String title = response.body().getTitle();
                    postUpdateStockOpnameCallback.onSuccess(title);
                }
            }

            @Override
            public void onFailure(Call<ResponsePostUpdateDataStockOpname> call, Throwable t) {
                postUpdateStockOpnameCallback.onError(t);
            }
        });
    }

    public static void GetUnit(Activity act, String id_item, final BaseApiService.GetUnitCallback getUnitCallback) {
        mApiService = ApiUtils.getAPIService();
        mApiService.getUnit(id_item).enqueue(new Callback<ResponseGetUnit>() {
            @Override
            public void onResponse(Call<ResponseGetUnit> call, Response<ResponseGetUnit> response) {
                getUnitCallback.onSuccess(response.body().getData());
            }

            @Override
            public void onFailure(Call<ResponseGetUnit> call, Throwable t) {
                getUnitCallback.onError(t);
            }
        });
    }
}
