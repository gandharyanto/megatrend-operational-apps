package com.megatrend.megatrendoperationalapps.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.megatrend.megatrendoperationalapps.R;
import com.megatrend.megatrendoperationalapps.activities.StockOpnameActivity;
import com.megatrend.megatrendoperationalapps.db.DatabaseHelper;
import com.megatrend.megatrendoperationalapps.object.AddStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.UnitObject;
import com.megatrend.megatrendoperationalapps.object.UpdateDetailSTOObject;
import com.megatrend.megatrendoperationalapps.utils.api.BaseApiService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 1/9/2018.
 */

public class DialogViews {

    public static class InputStockOpnameDialog {
        @BindView(R.id.ib_scan)
        ImageButton ibScan;
        @BindView(R.id.edtItemNo)
        EditText edtItemNo;
        @BindView(R.id.edtProductCode)
        AutoCompleteTextView edtProductCode;
        @BindView(R.id.edtProductName)
        EditText edtProductName;
        @BindView(R.id.edtStock)
        EditText edtStock;
        @BindView(R.id.edtRealStock)
        EditText edtRealStock;
        @BindView(R.id.btnOk)
        Button btnOk;
        @BindView(R.id.card_view)
        CardView cardView;
        BarcodeScanUtils.CheckPermission barcodeScanUtils = new BarcodeScanUtils.CheckPermission();
        DatabaseHelper databaseHelper;
        @BindView(R.id.rsSpinner)
        Spinner rsSpinner;
        String unit_id = "";
        String unit_conv = "";
        String unit_detail = "";
        List<String> itemList = new ArrayList<>();
        List<String> itemNoList = new ArrayList<>();

        public void displayInputDialog(final Activity act, final AddStockOpnameObject addStockOpnameObject,
                                       final List<UnitObject> unitObjects, final BaseApiService.PostAddStockOpnameCallback postAddStockOpnameCallback,
                                       final BaseApiService.PostAddStockOpnameCallback postItemNoCallback, final List<AddStockOpnameObject> itemObjectList) {
            final Dialog dialog = new Dialog(act);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            //dialog.setContentView(R.layout.dialog_input_stock_opname);
            View view = View.inflate(act, R.layout.dialog_input_stock_opname, null);
            dialog.setContentView(view);
            ButterKnife.bind(this, view);
            databaseHelper = new DatabaseHelper(act);
            final DialogMessage dialogMessage = new DialogMessage();
            if (itemObjectList != null) {
                itemList = new ArrayList<>();
                itemNoList = new ArrayList<>();
                for (int i = 0; i < itemObjectList.size(); i++) {
                    itemList.add(itemObjectList.get(i).getName());
                    itemNoList.add(String.valueOf(itemObjectList.get(i).getCode()));
                }

                ArrayAdapter adapter = new ArrayAdapter<>
                        (act, R.layout.spinner_text, itemNoList);
                //Getting the instance of AutoCompleteTextView
                edtProductCode.setThreshold(1);//will start working from first character
                edtProductCode.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                adapter.notifyDataSetChanged();

                edtProductCode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        /*String warehouseSelected = parent.getItemAtPosition(position).toString();
                        int indexSelected = warehouseList.indexOf(warehouseSelected);
                        warehouseID = warehouseIdList.get(indexSelected);
                        mWarehouse = warehouseList.get(position);
                        PreferenceManagers.setDataWithSameKey(Config.KEY_WAREHOUSE_INPUT, warehouseID, mContext);*/
                        edtProductCode.setFocusable(false);
                        edtProductCode.setFocusableInTouchMode(false);
                        edtProductCode.setClickable(false);
                        edtRealStock.requestFocus();
                    }
                });
            }

            if (addStockOpnameObject != null) {
                edtItemNo.setText(addStockOpnameObject.getItem_no());
                edtProductCode.setText(addStockOpnameObject.getCode());
                edtProductName.setText(addStockOpnameObject.getName());
                edtStock.setText(addStockOpnameObject.getQty_stock());
                edtRealStock.requestFocus();
            }

            if (unitObjects != null) {
                String[] unit = new String[unitObjects.size()];
                for (int i = 0; i < unitObjects.size(); i++) {
                    unit[i] = unitObjects.get(i).getUnit_name();
                }
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(act,
                        R.layout.spinner_text, unit);

                rsSpinner.setAdapter(spinnerArrayAdapter);
                edtItemNo.setClickable(false);
                edtItemNo.setFocusableInTouchMode(false);
                edtItemNo.setFocusable(false);
                edtRealStock.requestFocus();
            } else {
                String[] unit = new String[1];
                unit[0] = "Please Select";
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(act,
                        R.layout.spinner_text, unit);

                rsSpinner.setAdapter(spinnerArrayAdapter);
            }
            rsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (unitObjects != null) {
                        unit_id = unitObjects.get(position).getUnit_id();
                        unit_conv = unitObjects.get(position).getQtykali();
                        unit_detail = unitObjects.get(position).getUnit_name();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    //dialogMessage.displayMessage(act, "Not Complete", "please select unit");
                    if (unitObjects != null) {
                        unit_id = unitObjects.get(0).getUnit_id();
                        unit_conv = unitObjects.get(0).getQtykali();
                        unit_detail = unitObjects.get(0).getUnit_name();
                    }
                }
            });

            edtProductCode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        String item = edtProductCode.getText().toString();
                        boolean isItemInserted = databaseHelper.isItemInserted(item);
                        if (!isItemInserted) {
                            dialog.dismiss();
                            if (item != null) {
                                postItemNoCallback.onSuccess(item);
                            } else {
                                dialogMessage.displayMessage(act, "Error", "terjadi kesalahan");
                            }
                        } else {
                            dialog.dismiss();
                            dialogMessage.displayMessage(act, "Error", "Data Sudah Diinput");
                        }
                    }
                }
            });

            ibScan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    barcodeScanUtils.CheckPermission(act);
                }
            });

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (edtRealStock.getText().toString().equalsIgnoreCase("")) {
                        dialogMessage.displayMessage(act, "Error", "Please input real stock");
                    } else {
                        int qtyReal = Integer.parseInt(edtRealStock.getText().toString());
                        //int qtyStock = Integer.parseInt(addStockOpnameObject.getQty_stock());
                        //int qtyDiff = (qtyReal * Integer.parseInt(unit_conv)) - qtyStock;
                        addStockOpnameObject.setQty_real(edtRealStock.getText().toString());
                        addStockOpnameObject.setQty_diff(String.valueOf(0));
                        if (addStockOpnameObject != null) {
                            int dso_max_api_id = databaseHelper.getMaxIdDSO();
                            int dso_api_id = dso_max_api_id + 1;
                            databaseHelper.insertAddDetailStockOpname(String.valueOf(dso_api_id),
                                    PreferenceManagers.getData(Config.KEY_ID_API_ASO, act),
                                    addStockOpnameObject.getProduct_id(),
                                    addStockOpnameObject.getQty_stock(),
                                    addStockOpnameObject.getQty_real(),
                                    addStockOpnameObject.getQty_diff(),
                                    addStockOpnameObject.getItem_no(),
                                    null,
                                    addStockOpnameObject.getName(),
                                    addStockOpnameObject.getCode(),
                                    PreferenceManagers.getData(Config.KEY_ID_WAREHOUSE, act),
                                    PreferenceManagers.getData(Config.KEY_ID_EMPLOYEE, act),
                                    PreferenceManagers.getData(Config.KEY_ID_BRANCH, act),
                                    unit_id,
                                    unit_conv,
                                    unit_detail,
                                    "false");

                            Intent intent = new Intent(act, act.getClass());
                            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            act.startActivity(intent);
                            //itemView.dismiss();
                            //ApiUtils.PostAddStockOpname(act, addStockOpnameObject, postAddStockOpnameCallback);
                        } else {
                            dialogMessage.displayMessage(act, "Error", "item not found");
                        }
                    }
                    dialog.dismiss();
//                    act.finish();
                }
            });
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            dialog.show();
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);
            int width = Config.getWidth(act);
            dialog.getWindow().setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        }
    }

    public static class UpdateDetailStockOpname {
        @BindView(R.id.ib_scan)
        ImageButton ibScan;
        @BindView(R.id.edtItemNo)
        EditText edtItemNo;
        @BindView(R.id.edtProductCode)
        EditText edtProductCode;
        @BindView(R.id.edtProductName)
        EditText edtProductName;
        @BindView(R.id.edtStock)
        EditText edtStock;
        @BindView(R.id.edtRealStock)
        EditText edtRealStock;
        @BindView(R.id.btnOk)
        Button btnOk;
        @BindView(R.id.btnDel)
        Button btnDel;
        @BindView(R.id.card_view)
        CardView cardView;
        DatabaseHelper databaseHelper;
        @BindView(R.id.rsSpinner)
        Spinner rsSpinner;
        String unit_id, unit_conv, unit_detail;
        int spinnerPosition;

        public void DialogUpdateDetailStockOpname(final Activity act, final UpdateDetailSTOObject updateDetailSTOObject, final List<UnitObject> unitObjects) {
            final Dialog popUpDialog = new Dialog(act);
            popUpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            //dialog.setContentView(R.layout.dialog_input_stock_opname);
            View view = View.inflate(act, R.layout.dialog_update_stock_opname, null);
            popUpDialog.setContentView(view);
            ButterKnife.bind(this, view);

            databaseHelper = new DatabaseHelper(act);
            final DialogMessage dialogMessage = new DialogMessage();

            edtItemNo.setText(updateDetailSTOObject.getItem_no());
            edtProductCode.setText(updateDetailSTOObject.getCode());
            edtProductName.setText(updateDetailSTOObject.getName());
            //edtStock.setText(detailStockOpnameObject.getQty());
            edtRealStock.setText(updateDetailSTOObject.getQty_real());

            List<String> unit_idList = new ArrayList<>();
            /*if (unitObjects != null) {
                String[] unit = new String[unitObjects.size() + 1];
                String[] unit_id = new String[unitObjects.size() + 1];
                unit_id[0] = "0";
                unit[0] = "Please Select";
                unit_idList.add("0");
                for (int i = 1; i < unitObjects.size() + 1; i++) {
                    unit_idList.add(unitObjects.get(i - 1).getUnit_id());
                    unit[i] = unitObjects.get(i - 1).getUnit_name();
                    unit_id[i] = unitObjects.get(i - 1).getUnit_id();
                }
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(act,
                        R.layout.spinner_text, unit);

                rsSpinner.setAdapter(spinnerArrayAdapter);
                spinnerPosition = unit_idList.indexOf(updateDetailSTOObject.getUnit_id());
            } else {
                String[] unit = new String[1];
                unit[0] = "Please Select";
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(act,
                        R.layout.spinner_text, unit);

                rsSpinner.setAdapter(spinnerArrayAdapter);
                spinnerPosition = 0;
            }*/

            if (unitObjects != null) {
                String[] unit = new String[unitObjects.size()];
                unit[0] = "Please Select";
                //String[] unit = new String[unitObjects.size()];
                for (int i = 0; i < unitObjects.size(); i++) {
                    unit_idList.add(unitObjects.get(i).getUnit_id());
                    unit[i] = unitObjects.get(i).getUnit_name();
                }
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(act,
                        R.layout.spinner_text, unit);

                rsSpinner.setAdapter(spinnerArrayAdapter);
                edtItemNo.setClickable(false);
                edtItemNo.setFocusableInTouchMode(false);
                edtItemNo.setFocusable(false);
                edtRealStock.requestFocus();


                rsSpinner.setAdapter(spinnerArrayAdapter);
                spinnerPosition = unit_idList.indexOf(updateDetailSTOObject.getUnit_id());
            } else {
                String[] unit = new String[1];
                unit[0] = "Please Select";
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(act,
                        R.layout.spinner_text, unit);


                rsSpinner.setAdapter(spinnerArrayAdapter);
                spinnerPosition = 0;
            }

            rsSpinner.setSelection(spinnerPosition);
            rsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (unitObjects != null) {
                        unit_id = unitObjects.get(position).getUnit_id();
                        unit_conv = unitObjects.get(position).getQtykali();
                        unit_detail = unitObjects.get(position).getUnit_name();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    //dialogMessage.displayMessage(act, "Not Complete", "please select unit");
                    if (unitObjects != null) {
                        unit_id = unitObjects.get(0).getUnit_id();
                        unit_conv = unitObjects.get(0).getQtykali();
                        unit_detail = unitObjects.get(0).getUnit_name();
                    }
                }
            });

            btnDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    databaseHelper = new DatabaseHelper(act);
                                    databaseHelper.deleteDetailSTO(updateDetailSTOObject.getCode());
                                    DatabaseHelper.exportDB(act);

                                    Intent intent = new Intent(act, act.getClass());
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    act.startActivity(intent);

                                    dialog.dismiss();
                                    popUpDialog.dismiss();
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    dialog.dismiss();
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(act);
                    builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();
                }
            });

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (edtRealStock.getText().toString().equalsIgnoreCase("")) {
                        dialogMessage.displayMessage(act, "Error", "Please input real stock");
                    } else {
                        if (updateDetailSTOObject != null) {
                            int qtyReal = Integer.parseInt(edtRealStock.getText().toString());
//                            int qtyStock = Integer.parseInt(detailStockOpnameObject.getQty());
//                            int qtyDiff = (qtyReal * Integer.parseInt(detailStockOpnameObject.getUnit_conversion())) - qtyStock;
                            updateDetailSTOObject.setQty_real(edtRealStock.getText().toString());
                            databaseHelper.updateAddDetailStockOpname(String.valueOf(qtyReal), updateDetailSTOObject.getCode(), unit_conv, unit_detail, unit_id);

                            Intent intent = new Intent(act, act.getClass());
                            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            act.startActivity(intent);
                            //itemView.dismiss();
                            //ApiUtils.PostAddStockOpname(act, addStockOpnameObject, postAddStockOpnameCallback);
                        } else {
                            dialogMessage.displayMessage(act, "Error", "item not found");
                        }
                    }
                    popUpDialog.dismiss();
                }
            });

            popUpDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            popUpDialog.show();
            popUpDialog.setCancelable(true);
            popUpDialog.setCanceledOnTouchOutside(false);
            int width = Config.getWidth(act);
            popUpDialog.getWindow().setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            popUpDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        }
    }

    public static class DialogMessage {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvMessage)
        TextView tvMessage;
        @BindView(R.id.btnOk)
        Button btnOk;

        public void displayMessage(final Activity act, String title, String message) {
            // custom dialog
            final Dialog dialog = new Dialog(act);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_message);
            ButterKnife.bind(this, dialog);

            tvTitle.setText(title);
            tvMessage.setText(message);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                }
            });
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            dialog.show();
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);
            int width = Config.getWidth(act);
            dialog.getWindow().setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        }

        public void displayMessageWithFinish(final Activity act, String title, String message) {
            // custom dialog
            final Dialog dialog = new Dialog(act);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_message);
            ButterKnife.bind(this, dialog);

            tvTitle.setText(title);
            tvMessage.setText(message);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    act.finish();
                }
            });
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            dialog.show();
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);
            int width = Config.getWidth(act);
            dialog.getWindow().setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        }

        public void displayMessageIntentHome(final Activity act, String title, String message) {
            // custom dialog
            final Dialog dialog = new Dialog(act);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_message);
            ButterKnife.bind(this, dialog);

            tvTitle.setText(title);
            tvMessage.setText(message);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                    Intent i = new Intent(act, StockOpnameActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    act.startActivity(i);
                }
            });
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
            dialog.show();
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(false);
            int width = Config.getWidth(act);
            dialog.getWindow().setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        }
    }
}