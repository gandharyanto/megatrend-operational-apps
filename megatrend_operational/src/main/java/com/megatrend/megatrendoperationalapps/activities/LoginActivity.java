package com.megatrend.megatrendoperationalapps.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Time;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.megatrend.megatrendoperationalapps.R;
import com.megatrend.megatrendoperationalapps.component.ProgressDialogManager;
import com.megatrend.megatrendoperationalapps.db.DatabaseHelper;
import com.megatrend.megatrendoperationalapps.model.ResponsePostLoginUser;
import com.megatrend.megatrendoperationalapps.object.JsonLogin;
import com.megatrend.megatrendoperationalapps.object.StockOpnameDataObject;
import com.megatrend.megatrendoperationalapps.utils.Config;
import com.megatrend.megatrendoperationalapps.utils.DialogViews;
import com.megatrend.megatrendoperationalapps.utils.PreferenceManagers;
import com.megatrend.megatrendoperationalapps.utils.api.ApiUtils;
import com.megatrend.megatrendoperationalapps.utils.api.BaseApiService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by user on 1/16/2018.
 */

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.btnOk)
    Button btnOk;
    @BindView(R.id.edtUsername)
    EditText edtUsername;
    @BindView(R.id.edtPassword)
    EditText edtPassword;

    BaseApiService mApiService;
    DialogViews.DialogMessage dialogMessage;
    List<StockOpnameDataObject> stockOpnameDataObjectList;
    Context mContext;
    Activity mActivity;
    ProgressDialogManager dialog;
    private DatabaseHelper databaseHelper;
    private boolean isCheckPermissionHit = false;

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void CheckPermission() {
        if (!isCheckPermissionHit) {
            isCheckPermissionHit = true;
            String[] PERMISSIONS = {Manifest.permission.INTERNET,
                    Manifest.permission.ACCESS_NETWORK_STATE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE};
            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, 1);
            } else {

            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                boolean isGranted = true;
                for (int grant : grantResults) {
                    if (grant != PackageManager.PERMISSION_GRANTED) {
                        Timber.e("Mohon berikan akses ke semua permintaan");
                        isGranted = false;
                        finish();
                    }
                }

                if (isGranted) {

                }
            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (PreferenceManagers.hasData(Config.KEY_USERNAME, this)) {
            mApiService = ApiUtils.getAPIService();
            dialogMessage = new DialogViews.DialogMessage();
            stockOpnameDataObjectList = new ArrayList<>();
            databaseHelper = new DatabaseHelper(this);
            mContext = this;
            mActivity = this;
            dialog = new ProgressDialogManager(this);
            dialog.show();
            databaseHelper.dropAll_Table();
            PreferenceManagers.clearDataWithKey(Config.KEY_ADD_ONTOUCH, mContext);
            goToNext();
        } else {
            setContentView(R.layout.activity_login);
            ButterKnife.bind(this);

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

            mApiService = ApiUtils.getAPIService();
            dialogMessage = new DialogViews.DialogMessage();
            stockOpnameDataObjectList = new ArrayList<>();
            databaseHelper = new DatabaseHelper(this);
            mContext = this;
            mActivity = this;
            dialog = new ProgressDialogManager(this);
            databaseHelper.dropAll_Table();

            edtUsername.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                    if (id == EditorInfo.IME_ACTION_NEXT) {
                        edtPassword.requestFocus();
                        return true;
                    }
                    return false;
                }
            });

            edtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                    if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                        btnOk.performClick();
                        return true;
                    }
                    return false;
                }
            });

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.show();

                    //if (ApiUtils.isURLReachable(mContext, "http://google.com")) {
                    JsonLogin jsonLogin = new JsonLogin(edtUsername.getText().toString(), edtPassword.getText().toString());
                    ApiUtils.PostLoginUser(mActivity, jsonLogin, new BaseApiService.PostLoginUserCallback() {
                        @Override
                        public void onSuccess(@NonNull ResponsePostLoginUser value) {
                            String[] username = edtUsername.getText().toString().split("@");
                            PreferenceManagers.setDataWithSameKey(Config.KEY_USERNAME, username[0], mContext);
                            PreferenceManagers.setDataWithSameKey(Config.KEY_USERMAIL, edtUsername.getText().toString(), mContext);
                            PreferenceManagers.setDataWithSameKey(Config.KEY_ID_BRANCH, value.getIdBranch(), mContext);
                            PreferenceManagers.setDataWithSameKey(Config.KEY_ID_SALESMAN, value.getIdSalesman(), mContext);
                            goToNext();
                        }

                        @Override
                        public void onError(@NonNull Throwable t) {
                            if (dialog.isShowing())
                                dialog.dismiss();
                            dialogMessage.displayMessage(mActivity, "Error", "Gagal Login\n\n" + t.getMessage());
                        }
                    });
                    /*} else {
                        if (dialog.isShowing())
                            dialog.dismiss();
                        dialogMessage.displayMessage(mActivity, "Error", "No Connection");
                    }*/
                }
            });

            CheckPermission();
        }
    }

    public void goToNext() {
        if (dialog.isShowing())
            dialog.dismiss();

        DatabaseHelper.exportDB(mActivity);
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        PreferenceManagers.setDataWithSameKey(Config.KEY_LAST_LOGIN, today.monthDay + "", LoginActivity.this);

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
