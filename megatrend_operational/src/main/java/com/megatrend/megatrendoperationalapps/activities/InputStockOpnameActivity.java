package com.megatrend.megatrendoperationalapps.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.megatrend.megatrendoperationalapps.R;
import com.megatrend.megatrendoperationalapps.adapter.CustomTableDataAdapter;
import com.megatrend.megatrendoperationalapps.adapter.CustomTableHeaderAdapter;
import com.megatrend.megatrendoperationalapps.component.ProgressDialogManager;
import com.megatrend.megatrendoperationalapps.db.DatabaseHelper;
import com.megatrend.megatrendoperationalapps.object.AddStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.BranchObject;
import com.megatrend.megatrendoperationalapps.object.DetailStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.ProductObject;
import com.megatrend.megatrendoperationalapps.object.StockOpnameDataObject;
import com.megatrend.megatrendoperationalapps.object.SubmitStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.UnitObject;
import com.megatrend.megatrendoperationalapps.object.UpdateDetailSTOObject;
import com.megatrend.megatrendoperationalapps.object.WarehouseObject;
import com.megatrend.megatrendoperationalapps.utils.Config;
import com.megatrend.megatrendoperationalapps.utils.DialogViews;
import com.megatrend.megatrendoperationalapps.utils.PreferenceManagers;
import com.megatrend.megatrendoperationalapps.utils.api.ApiUtils;
import com.megatrend.megatrendoperationalapps.utils.api.BaseApiService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.listeners.TableDataClickListener;
import timber.log.Timber;

/**
 * Created by user on 1/6/2018.
 */

public class InputStockOpnameActivity extends BaseActivity {

    @BindView(R.id.actBranch)
    AutoCompleteTextView actBranch;
    @BindView(R.id.actWarehouse)
    AutoCompleteTextView actWarehouse;
    @Nullable
    @BindView(R.id.actDate)
    AutoCompleteTextView actDate;
    @BindView(R.id.tableViewAddStockOpname)
    TableView<String[]> tableViewAddStockOpname;

    Context mContext;
    Activity mActivity;
    ProgressDialogManager loadingDialog;
    List<BranchObject> branchObjectList = new ArrayList<>();
    List<WarehouseObject> warehouseObjectList = new ArrayList<>();
    List<AddStockOpnameObject> itemObjectList = new ArrayList<>();
    List<AddStockOpnameObject> addStockOpnameObjectList = new ArrayList<>();
    List<DetailStockOpnameObject> detailStockOpnameObjectList = new ArrayList<>();
    List<String> branchList = new ArrayList<>();
    List<String> warehouseList = new ArrayList<>();
    List<String> branchIdList = new ArrayList<>();
    List<String> warehouseIdList = new ArrayList<>();
    BaseApiService mApiService;
    StockOpnameDataObject stockOpnameDataObject;
    String branchID, warehouseID, inputDate, mWarehouse;
    DialogViews.InputStockOpnameDialog inputStockOpnameDialog = new DialogViews.InputStockOpnameDialog();
    DialogViews.UpdateDetailStockOpname updateDetailStockOpname = new DialogViews.UpdateDetailStockOpname();
    DialogViews.DialogMessage dialogMessage = new DialogViews.DialogMessage();
    boolean hasExtra = false;
    boolean addOnItemTouch = false;
    DatabaseHelper databaseHelper;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.llactBranch)
    LinearLayout llactBranch;
    @BindView(R.id.llactWarehouse)
    LinearLayout llactWarehouse;
    @BindView(R.id.llactDate)
    LinearLayout llactDate;
    @BindView(R.id.btnSubmit)
    ImageView btnSubmit;
    String[] headerDStockOpname = {"Item No", "Code", "Qty", "Unit"};
    String[][] dataDStockOpname;

    BaseApiService.PostAddStockOpnameCallback postAddStockOpnameCallback = new BaseApiService.PostAddStockOpnameCallback() {

        @Override
        public void onSuccess(@NonNull String value) {
            dialogMessage.displayMessageWithFinish(InputStockOpnameActivity.this, value, "");
        }

        @Override
        public void onError(@NonNull Throwable throwable) {
            dialogMessage.displayMessage(InputStockOpnameActivity.this, "Fail", "");
        }
    };

    BaseApiService.PostAddStockOpnameCallback postItemNoCallback = new BaseApiService.PostAddStockOpnameCallback() {
        @Override
        public void onSuccess(@NonNull String value) {
            if (!value.equalsIgnoreCase("")) {
                PreferenceManagers.clearDataWithKey(Config.KEY_INPUT_MANUAL, mContext);
                boolean isItemInserted = databaseHelper.isItemInserted(value);
                if (!isItemInserted) {
                    getProductByItem(branchID, PreferenceManagers.getData(Config.KEY_ID_WAREHOUSE, mContext), value);
                } else {
                    dialogMessage.displayMessage(InputStockOpnameActivity.this, "Fail", "Data Already Inserted");
                }
            } else {
                inputStockOpnameDialog.displayInputDialog(getActivity(), null, null, postAddStockOpnameCallback, postItemNoCallback, null);
            }
        }

        @Override
        public void onError(@NonNull Throwable throwable) {
            dialogMessage.displayMessage(InputStockOpnameActivity.this, "Fail", "");
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mContext = this;
        mActivity = this;
        mApiService = ApiUtils.getAPIService();
        databaseHelper = new DatabaseHelper(this);
        branchObjectList = new ArrayList<>();
        warehouseObjectList = new ArrayList<>();
        addStockOpnameObjectList = new ArrayList<>();
        detailStockOpnameObjectList = new ArrayList<>();
        branchList = new ArrayList<>();
        warehouseList = new ArrayList<>();
        branchIdList = new ArrayList<>();
        warehouseIdList = new ArrayList<>();
        loadingDialog = new ProgressDialogManager(this);

        PreferenceManagers.clearDataWithKey(Config.KEY_BRANCH_INPUT, this);
        PreferenceManagers.clearDataWithKey(Config.KEY_DETAIL_SO_INPUT, this);
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            String status = intent.getStringExtra("status");
            if (status.equalsIgnoreCase("2")) {
                actWarehouse.setText(PreferenceManagers.getData(Config.KEY_WAREHOUSE_INPUT, mContext));
                actWarehouse.setFocusableInTouchMode(false);
                actWarehouse.setClickable(false);
                actWarehouse.setFocusable(false);
            }
        }

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        actDate.setText(dateFormat.format(date));
        inputDate = dateFormat.format(date);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                barcodeScanUtils.CheckPermission(getActivity());
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Apakah Anda Ingin Melakukan Sync Product?")
                .setCancelable(false)
                .setPositiveButton("Ya",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dg,
                                                int id) {
                                getProductByItem1(branchID, PreferenceManagers.getData(Config.KEY_ID_WAREHOUSE, mContext), "");
                            }
                        })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int id) {
                        dialog.dismiss();
                    }
                }).show();
    }

    @Override
    protected Activity getActivity() {
        return this;
    }

    @Override
    protected String getTvTitle() {
        return "Stock Opname Details";
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_input_stock_opname;
    }

    @Override
    protected Boolean isShowAddToolbar() {
        return false;
    }

    @Override
    protected Boolean isShowNextToolbar() {
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Timber.d("Scan canceled");
            } else {
                boolean isItemInserted = databaseHelper.isItemInserted(result.getContents());
                if (!isItemInserted) {
                    getProductByItem(branchID, warehouseID, result.getContents());
                    Timber.d(result.getContents());
                } else {
                    dialogMessage.displayMessage(mActivity, "Error", "Data Sudah Diinput");
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PreferenceManagers.hasData(Config.KEY_INPUT_MANUAL, mContext)) {
            loadingDialog.show();
            PreferenceManagers.clearDataWithKey(Config.KEY_INPUT_MANUAL, mContext);

            List<AddStockOpnameObject> value = databaseHelper.getTableProduct();
            if (value.size() != 0) {
                try {
                    itemObjectList = value;
                    value.get(0).setDate(inputDate);
                    value.get(0).setWarehouse(mWarehouse);
                    value.get(0).setWarehouse_id(warehouseID);
                    value.get(0).setBranch_id(branchID);
                    String addStockOpnameObjectJson = (new Gson().toJson(value));
                    PreferenceManagers.setDataWithSameKey(Config.KEY_DETAIL_SO_INPUT, addStockOpnameObjectJson, mContext);

                    final List<AddStockOpnameObject> addStockOpnameObject = value;
                    if (loadingDialog.isShowing())
                        loadingDialog.dismiss();
                    inputStockOpnameDialog.displayInputDialog(getActivity(), null, null, postAddStockOpnameCallback, postItemNoCallback, itemObjectList);
                } catch (Exception e) {
                    if (loadingDialog.isShowing())
                        loadingDialog.dismiss();
                    Crashlytics.setString("277", e.getMessage());
                    dialogMessage.displayMessageWithFinish(mActivity, "Error", e.getMessage());
                }
            } else {
                if (loadingDialog.isShowing())
                    loadingDialog.dismiss();
                dialogMessage.displayMessage(InputStockOpnameActivity.this, "Error", "No data found");
                Timber.e("Gagal mengambil getdataProductbyItem - 1 (" + value + ")");
            }
        } else {
            databaseHelper = new DatabaseHelper(this);
            if (hasExtra) {
                tableViewAddStockOpname.setColumnCount(4);
                tableViewAddStockOpname.setColumnWeight(0, 5);
                tableViewAddStockOpname.setColumnWeight(1, 10);
                tableViewAddStockOpname.setColumnWeight(2, 3);
                tableViewAddStockOpname.setColumnWeight(3, 3);
                tableViewAddStockOpname.setHeaderBackgroundColor(Color.parseColor("#FFC107"));
                dataDStockOpname = databaseHelper.getTableDetailStockOpnameArray(stockOpnameDataObject.getId(), false);
                tableViewAddStockOpname.setHeaderAdapter(new CustomTableHeaderAdapter(this, headerDStockOpname));
                tableViewAddStockOpname.setDataAdapter(new CustomTableDataAdapter(this, dataDStockOpname));
            } else {
                tableViewAddStockOpname.setColumnCount(4);
                tableViewAddStockOpname.setColumnWeight(0, 5);
                tableViewAddStockOpname.setColumnWeight(1, 10);
                tableViewAddStockOpname.setColumnWeight(2, 3);
                tableViewAddStockOpname.setColumnWeight(3, 3);
                tableViewAddStockOpname.setHeaderBackgroundColor(Color.parseColor("#FFC107"));
                dataDStockOpname = databaseHelper.getTableDetailStockOpnameArray(String.valueOf(PreferenceManagers.getData(Config.KEY_ID_API_ASO, mContext)), false);
                tableViewAddStockOpname.setHeaderAdapter(new CustomTableHeaderAdapter(this, headerDStockOpname));
                tableViewAddStockOpname.setDataAdapter(new CustomTableDataAdapter(this, dataDStockOpname));
            }

            if (!addOnItemTouch) {
                addOnItemTouch = true;
                tableViewAddStockOpname.addDataClickListener(new TableDataClickListener<String[]>() {
                    @Override
                    public void onDataClicked(int rowIndex, String[] clickedData) {
                        /*Timber.i(clickedData[0]);
                        Toast.makeText(InputStockOpnameActivity.this, clickedData[0], Toast.LENGTH_SHORT).show();*/

                        final UpdateDetailSTOObject updateDetailSTOObject = databaseHelper.getUpdateDetailSTO(clickedData[1]);

                        ApiUtils.GetUnit(mActivity, updateDetailSTOObject.getCode(), new BaseApiService.GetUnitCallback() {
                            @Override
                            public void onSuccess(@NonNull List<UnitObject> value) {
                                updateDetailStockOpname.DialogUpdateDetailStockOpname(mActivity, updateDetailSTOObject, value);
                                //inputStockOpnameDialog.displayInputDialog(mActivity, upa, value, postAddStockOpnameCallback, null, null);
                            }

                            @Override
                            public void onError(@NonNull Throwable throwable) {
                                Crashlytics.setString("321", throwable.getMessage());
                                dialogMessage.displayMessage(InputStockOpnameActivity.this, "Error", throwable.getMessage());
                            }
                        });
                    }
                });
            }

            Boolean actWarehouseBool = PreferenceManagers.hasData(Config.KEY_WAREHOUSE_INPUT, getActivity());
            if (actWarehouseBool) {
                actWarehouse.setFocusableInTouchMode(false);
                actWarehouse.setClickable(false);
                actWarehouse.setFocusable(false);
                actWarehouse.setKeyListener(null);
                btnSubmit.setVisibility(View.VISIBLE);
            }
            getBranch();
        }
    }

    protected void getBranch() {
        loadingDialog.show();

        branchID = PreferenceManagers.getData(Config.KEY_ID_BRANCH, mContext);
        PreferenceManagers.setDataWithSameKey(Config.KEY_BRANCH_INPUT, branchID, mContext);
        getWarehouse(branchID);
    }

    protected void getWarehouse(String branch) {

        warehouseObjectList = databaseHelper.getWarehouseData(branch);
        if (warehouseList != null) {
            for (int i = 0; i < warehouseObjectList.size(); i++) {
                warehouseList.add(warehouseObjectList.get(i).getName());
                warehouseIdList.add(String.valueOf(warehouseObjectList.get(i).getId()));
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<>
                    (mContext, android.R.layout.select_dialog_item, warehouseList);
            actWarehouse.setThreshold(1);
            actWarehouse.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            actWarehouse.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String warehouseSelected = parent.getItemAtPosition(position).toString();
                    int indexSelected = warehouseList.indexOf(warehouseSelected);
                    warehouseID = warehouseIdList.get(indexSelected);
                    mWarehouse = warehouseList.get(position);
                    PreferenceManagers.setDataWithSameKey(Config.KEY_WAREHOUSE_INPUT, warehouseID, mContext);
                }
            });

            if (hasExtra) {
                int index = warehouseIdList.indexOf(stockOpnameDataObject.getWarehouse_id());
                if (index != -1) {
                    warehouseID = stockOpnameDataObject.getWarehouse_id();
                    mWarehouse = warehouseList.get(index);
                    actWarehouse.setText(warehouseList.get(index));
                    actWarehouse.performCompletion();
                    PreferenceManagers.setDataWithSameKey(Config.KEY_WAREHOUSE_INPUT, warehouseID, mContext);

                    ApiUtils.getDetailStockOpname(mActivity, stockOpnameDataObject.getId(), new BaseApiService.GetDetailStockOpnameCallback() {
                        @Override
                        public void onSuccess(@NonNull List<DetailStockOpnameObject> value) {
                            if (value.size() > 0) {
                                detailStockOpnameObjectList = value;

                                databaseHelper.dropDetail_Stock_Opname_Table();
                                for (int i = 0; i < detailStockOpnameObjectList.size(); i++) {
                                    databaseHelper.insertDetailStockOpname(detailStockOpnameObjectList.get(i).getId(),
                                            PreferenceManagers.getData(Config.KEY_ID_API_ASO, mContext),
                                            detailStockOpnameObjectList.get(i).getProduct_id(),
                                            detailStockOpnameObjectList.get(i).getQty(),
                                            detailStockOpnameObjectList.get(i).getQty(),
                                            detailStockOpnameObjectList.get(i).getDeference(),
                                            detailStockOpnameObjectList.get(i).getItem_no(),
                                            detailStockOpnameObjectList.get(i).getDeleted_at(),
                                            detailStockOpnameObjectList.get(i).getName(),
                                            detailStockOpnameObjectList.get(i).getCode(),
                                            PreferenceManagers.getData(Config.KEY_ID_WAREHOUSE, mContext),
                                            PreferenceManagers.getData(Config.KEY_ID_EMPLOYEE, mContext),
                                            PreferenceManagers.getData(Config.KEY_ID_BRANCH, mContext),
                                            detailStockOpnameObjectList.get(i).getUnit_id(),
                                            detailStockOpnameObjectList.get(i).getUnit_conversion(),
                                            detailStockOpnameObjectList.get(i).getUnitname(),
                                            "true");
                                }
                                tableViewAddStockOpname.setColumnCount(4);
                                tableViewAddStockOpname.setColumnWeight(0, 5);
                                tableViewAddStockOpname.setColumnWeight(1, 10);
                                tableViewAddStockOpname.setColumnWeight(2, 3);
                                tableViewAddStockOpname.setColumnWeight(3, 3);
                                tableViewAddStockOpname.setHeaderBackgroundColor(Color.parseColor("#FFC107"));
                                dataDStockOpname = databaseHelper.getTableDetailStockOpnameArray(stockOpnameDataObject.getId(),false);
                                tableViewAddStockOpname.setHeaderAdapter(new CustomTableHeaderAdapter(mActivity, headerDStockOpname));
                                tableViewAddStockOpname.setDataAdapter(new CustomTableDataAdapter(mActivity, dataDStockOpname));

                                if (loadingDialog.isShowing()) {
                                    loadingDialog.dismiss();
                                }
                            } else {
                                tableViewAddStockOpname.setColumnCount(4);
                                tableViewAddStockOpname.setColumnWeight(0, 5);
                                tableViewAddStockOpname.setColumnWeight(1, 10);
                                tableViewAddStockOpname.setColumnWeight(2, 3);
                                tableViewAddStockOpname.setColumnWeight(3, 3);
                                tableViewAddStockOpname.setHeaderBackgroundColor(Color.parseColor("#FFC107"));
                                dataDStockOpname = databaseHelper.getTableDetailStockOpnameArray(stockOpnameDataObject.getId(),false);
                                tableViewAddStockOpname.setHeaderAdapter(new CustomTableHeaderAdapter(mActivity, headerDStockOpname));
                                tableViewAddStockOpname.setDataAdapter(new CustomTableDataAdapter(mActivity, dataDStockOpname));
                                if (!addOnItemTouch) {
                                    addOnItemTouch = true;
                                    tableViewAddStockOpname.addDataClickListener(new TableDataClickListener<String[]>() {
                                        @Override
                                        public void onDataClicked(int rowIndex, String[] clickedData) {
                                            Toast.makeText(InputStockOpnameActivity.this, clickedData[0], Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }

                                if (loadingDialog.isShowing()) {
                                    loadingDialog.dismiss();
                                }
                                Timber.e("Gagal mengambil data detail stock opname - 1 (" + value + ")");
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable t) {

                            if (loadingDialog.isShowing()) {
                                loadingDialog.dismiss();
                            }
                            Crashlytics.setString("447", t.getMessage());
                            dialogMessage.displayMessage(InputStockOpnameActivity.this, "Error", t.getMessage());
                            Timber.e("Gagal mengambil data detail stock opname - 2 (" + t.getMessage() + ")");
                        }
                    });
                } else {
                    dialogMessage.displayMessageWithFinish(InputStockOpnameActivity.this, "Error", "Not Your Warehouse");
                }

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            } else {

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        }
    }

    protected void getProductByItem1(final String branch, final String warehouse, final String itemNo) {
        loadingDialog.show();

        ApiUtils.getProductbyItem(mActivity, branch, warehouse, itemNo, new BaseApiService.GetProductDetailCallback() {

            @Override
            public void onSuccess(@NonNull List<ProductObject> value) {
                if (value.size() != 0) {
                    List<ProductObject> productObjects = value;
                    new insertData().execute(productObjects);
                } else {
                    if (loadingDialog.isShowing()) {
                        loadingDialog.dismiss();
                    }
                    dialogMessage.displayMessage(mActivity, "Data Tidak Diterima", "");
                }
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                Crashlytics.setString("491", throwable.getMessage());
                dialogMessage.displayMessage(mActivity, "Data Tidak Diterima", throwable.getMessage());
            }
        });
    }

    protected void getProductByItem(final String branch, final String warehouse, final String itemNo) {
        List<AddStockOpnameObject> valueItem = databaseHelper.getTableProductByCode(itemNo);
        if (valueItem.size() > 0) {
            try {
                valueItem.get(0).setDate(inputDate);
                valueItem.get(0).setWarehouse(mWarehouse);
                valueItem.get(0).setWarehouse_id(warehouse);
                valueItem.get(0).setBranch_id(branch);
                String addStockOpnameObjectJson = (new Gson().toJson(valueItem));
                PreferenceManagers.setDataWithSameKey(Config.KEY_DETAIL_SO_INPUT, addStockOpnameObjectJson, mContext);

                final List<AddStockOpnameObject> addStockOpnameObject = valueItem;
                ApiUtils.GetUnit(mActivity, addStockOpnameObject.get(0).getCode(), new BaseApiService.GetUnitCallback() {
                    @Override
                    public void onSuccess(@NonNull List<UnitObject> value) {
                        inputStockOpnameDialog.displayInputDialog(mActivity, addStockOpnameObject.get(0), value, postAddStockOpnameCallback, null, null);
                    }

                    @Override
                    public void onError(@NonNull Throwable throwable) {
                        Crashlytics.setString("517", throwable.getMessage());
                        dialogMessage.displayMessage(InputStockOpnameActivity.this, "Error", throwable.getMessage());
                    }
                });
            } catch (Exception e) {
                Crashlytics.setString("522", e.getMessage());
                dialogMessage.displayMessageWithFinish(mActivity, "Error", e.getMessage() + "\n516");
            }
        } else {
            dialogMessage.displayMessage(InputStockOpnameActivity.this, "Fail", "Data Not Found\n" + itemNo);
        }


        if (loadingDialog.isShowing()) {
            loadingDialog.dismiss();
        }
    }

    public void submitStockOpname(final Context mContext, final Activity mActivity) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        databaseHelper = new DatabaseHelper(mContext);
                        int aso_max_api_id = databaseHelper.getMaxIdASO();

                        /*if (Boolean.valueOf(PreferenceManagers.getData(Config.KEY_UPDATE, mContext))) {
                            List<SubmitStockOpnameObject> submitStockOpnameObjectList = databaseHelper.getTableJoinStockAndDetail(stockOpnameDataObject.getId());
                            Map<String, String> data = new HashMap<>();
                            data.put("date", submitStockOpnameObjectList.get(0).getDate());
                            data.put("branch_id", submitStockOpnameObjectList.get(0).getBranch_id());
                            data.put("warehouse_id", submitStockOpnameObjectList.get(0).getWarehouse_id());
                            data.put("updated_by", "1");
                            data.put("platform", "android");
                            data.put("delitem", "");
                            String id_detail = "";
                            String productid = "";
                            String item_no = "";
                            String unit_id = "";
                            String qty = "";
                            String unit_conversion = "";
                            for (int i = 0; i < submitStockOpnameObjectList.size(); i++) {
                                id_detail = id_detail + submitStockOpnameObjectList.get(i).getDso_api_id() + ",";
                                productid = productid + submitStockOpnameObjectList.get(i).getProductid() + ",";
                                item_no = item_no + submitStockOpnameObjectList.get(i).getItem_no() + ",";
                                unit_id = unit_id + submitStockOpnameObjectList.get(i).getUnit_id() + ",";
                                qty = qty + submitStockOpnameObjectList.get(i).getQtyreal() + ",";
                                unit_conversion = unit_conversion + submitStockOpnameObjectList.get(i).getUnit_conv() + ",";
                            }

                            id_detail = id_detail.substring(0, id_detail.length() - 1);
                            productid = productid.substring(0, productid.length() - 1);
                            item_no = item_no.substring(0, item_no.length() - 1);
                            unit_id = unit_id.substring(0, unit_id.length() - 1);
                            qty = qty.substring(0, qty.length() - 1);
                            unit_conversion = unit_conversion.substring(0, unit_conversion.length() - 1);

                            data.put("id_detail", id_detail);
                            data.put("productid", productid);
                            data.put("item_no", item_no);
                            data.put("unit_id", unit_id);
                            data.put("qty", qty);
                            data.put("unit_conversion", unit_conversion);

                            ApiUtils.PostUpdateStockOpname(mActivity, submitStockOpnameObjectList.get(0).getAso_api_id(), data, new BaseApiService.PostUpdateStockOpnameCallback() {
                                @Override
                                public void onSuccess(@NonNull String value) {
                                    DatabaseHelper.exportDB();
                                    databaseHelper.dropAll_Table();
                                    dialogMessage.displayMessageWithFinish(mActivity, value, "");
                                }

                                @Override
                                public void onError(@NonNull Throwable throwable) {

                                }
                            });

                        } else {*/
                        List<SubmitStockOpnameObject> submitStockOpnameObjectList = databaseHelper.getTableJoinStockAndDetail(PreferenceManagers.getData(Config.KEY_ID_API_ASO, mContext));

                        Map<String, String> data = new HashMap<>();
                        data.put("stock_opname_id", submitStockOpnameObjectList.get(0).getAso_api_id());
                        data.put("platform", "Android");
                        data.put("date", submitStockOpnameObjectList.get(0).getDate());

                        String productid = "";
                        String qty = "";
                        String qtyreal = "";
                        String item_no = "";
                        String unit_id = "";
                        String unit_conv = "";
                        String employee_id = "";
                        if (submitStockOpnameObjectList.get(0).getProductid() != null) {
                            for (int i = 0; i < submitStockOpnameObjectList.size(); i++) {
                                productid = productid + submitStockOpnameObjectList.get(i).getProductid() + ",";
                                qty = qty + submitStockOpnameObjectList.get(i).getQty() + ",";
                                qtyreal = qtyreal + submitStockOpnameObjectList.get(i).getQtyreal() + ",";
                                item_no = item_no + submitStockOpnameObjectList.get(i).getItem_no() + ",";
                                unit_id = unit_id + submitStockOpnameObjectList.get(i).getUnit_id() + ",";
                                unit_conv = unit_conv + submitStockOpnameObjectList.get(i).getUnit_conv() + ",";
                                employee_id = employee_id + submitStockOpnameObjectList.get(i).getEmployee_id() + ",";
                            }

                            productid = productid.substring(0, productid.length() - 1);
                            qty = qty.substring(0, qty.length() - 1);
                            qtyreal = qtyreal.substring(0, qtyreal.length() - 1);
                            item_no = item_no.substring(0, item_no.length() - 1);
                            unit_id = unit_id.substring(0, unit_id.length() - 1);
                            unit_conv = unit_conv.substring(0, unit_conv.length() - 1);
                            employee_id = employee_id.substring(0, employee_id.length() - 1);
                            ApiUtils.PostAddStockOpname2(mActivity, data, employee_id, productid, qtyreal, unit_conv, unit_id, new BaseApiService.PostAddStockOpnameCallback() {
                                @Override
                                public void onSuccess(@NonNull String value) {
                                    DatabaseHelper.exportDB(mActivity);
                                    databaseHelper.dropAll_Table();
                                    dialogMessage.displayMessageIntentHome(mActivity, value, "");
                                    finish();
                                }

                                @Override
                                public void onError(@NonNull Throwable throwable) {
                                    Crashlytics.setString("640", throwable.getMessage());
                                    dialogMessage.displayMessage(mActivity, "Error", throwable.getMessage());
                                }
                            });
                        } else {
                            dialogMessage.displayMessage(mActivity, "Error", "Please Add Stock");
                        }
//                        }
                        dialog.dismiss();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
        DatabaseHelper.exportDB(mActivity);
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        DatabaseHelper.exportDB(mActivity);
                        dialog.dismiss();
                        finish();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    protected class insertData extends AsyncTask<List<ProductObject>, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(List<ProductObject>... params) {
            databaseHelper.dropProduct();

            if (params[0].size() != 0) {
                for (int i = 0; i < params[0].size(); i++) {
                    databaseHelper.insertProduct(params[0].get(i).getProduct_id(),
                            params[0].get(i).getItem_no(),
                            params[0].get(i).getCode(),
                            params[0].get(i).getName(),
                            params[0].get(i).getStockavl());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
            }
            dialogMessage.displayMessage(mActivity, "Success", "Sync Product Done");
        }
    }
}
