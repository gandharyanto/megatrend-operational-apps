package com.megatrend.megatrendoperationalapps.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.megatrend.megatrendoperationalapps.R;
import com.sa90.materialarcmenu.ArcMenu;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @Override
    protected Activity getActivity() {
        return this;
    }

    @Override
    protected String getTvTitle() {
        return getString(R.string.title_app);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    protected Boolean isShowAddToolbar() {
        return false;
    }

    @Override
    protected Boolean isShowNextToolbar() {
        return false;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

    }
}
