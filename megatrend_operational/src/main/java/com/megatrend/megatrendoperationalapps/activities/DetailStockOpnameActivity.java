package com.megatrend.megatrendoperationalapps.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.megatrend.megatrendoperationalapps.R;
import com.megatrend.megatrendoperationalapps.adapter.CustomTableDataAdapter;
import com.megatrend.megatrendoperationalapps.adapter.CustomTableHeaderAdapter;
import com.megatrend.megatrendoperationalapps.adapter.StockOpnameDetailAdapter;
import com.megatrend.megatrendoperationalapps.component.ProgressDialogManager;
import com.megatrend.megatrendoperationalapps.db.DatabaseHelper;
import com.megatrend.megatrendoperationalapps.object.AddStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.BranchObject;
import com.megatrend.megatrendoperationalapps.object.DetailStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.StockOpnameDataObject;
import com.megatrend.megatrendoperationalapps.object.UnitObject;
import com.megatrend.megatrendoperationalapps.object.WarehouseObject;
import com.megatrend.megatrendoperationalapps.utils.Config;
import com.megatrend.megatrendoperationalapps.utils.DialogViews;
import com.megatrend.megatrendoperationalapps.utils.PreferenceManagers;
import com.megatrend.megatrendoperationalapps.utils.api.ApiUtils;
import com.megatrend.megatrendoperationalapps.utils.api.BaseApiService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.listeners.TableDataClickListener;
import timber.log.Timber;

/**
 * Created by user on 3/19/2018.
 */

public class DetailStockOpnameActivity extends BaseActivity {
    @BindView(R.id.actBranch)
    AutoCompleteTextView actBranch;
    @BindView(R.id.actWarehouse)
    AutoCompleteTextView actWarehouse;
    @Nullable
    @BindView(R.id.actDate)
    AutoCompleteTextView actDate;
    @Nullable
    @BindView(R.id.tableViewAddStockOpname)
    TableView tableViewAddStockOpname;


    Context mContext;
    Activity mActivity;
    List<BranchObject> branchObjectList = new ArrayList<>();
    List<WarehouseObject> warehouseObjectList = new ArrayList<>();
    List<AddStockOpnameObject> addStockOpnameObjectList = new ArrayList<>();
    List<DetailStockOpnameObject> detailStockOpnameObjectList = new ArrayList<>();
    List<String> branchList = new ArrayList<>();
    List<String> warehouseList = new ArrayList<>();
    List<String> branchIdList = new ArrayList<>();
    List<String> warehouseIdList = new ArrayList<>();
    BaseApiService mApiService;
    /*AllBranchAdapter allBranchAdapter;*/
    StockOpnameDetailAdapter stockOpnameDetailAdapter;
    DetailStockOpnameObject detailStockOpnameObject;
    StockOpnameDataObject stockOpnameDataObject;
    String branchID, warehouseID, inputDate, mWarehouse;
    DialogViews.InputStockOpnameDialog inputStockOpnameDialog = new DialogViews.InputStockOpnameDialog();
    DialogViews.UpdateDetailStockOpname updateDetailStockOpname = new DialogViews.UpdateDetailStockOpname();
    DialogViews.DialogMessage dialogMessage = new DialogViews.DialogMessage();
    boolean hasExtra = false;
    boolean detailExtra = false;
    boolean addOnItemTouch = false;
    DatabaseHelper databaseHelper;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.llactBranch)
    LinearLayout llactBranch;
    @BindView(R.id.llactWarehouse)
    LinearLayout llactWarehouse;
    @BindView(R.id.llactDate)
    LinearLayout llactDate;
    @BindView(R.id.btnSubmit)
    ImageView btnSubmit;
    String[] headerDStockOpname = {"Item No", "Name", "Code", "Qty", "Unit", "Unit Conversion"};
    String[][] dataDStockOpname;
    ProgressDialogManager dialog;

    BaseApiService.PostAddStockOpnameCallback postAddStockOpnameCallback = new BaseApiService.PostAddStockOpnameCallback() {

        @Override
        public void onSuccess(@NonNull String value) {
            dialogMessage.displayMessageWithFinish(DetailStockOpnameActivity.this, value, "");
        }

        @Override
        public void onError(@NonNull Throwable throwable) {
            dialogMessage.displayMessage(DetailStockOpnameActivity.this, "Fail", "");
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mContext = this;
        mActivity = this;
        addOnItemTouch = true;
        mApiService = ApiUtils.getAPIService();
        databaseHelper = new DatabaseHelper(this);
        dialog = new ProgressDialogManager(this);
        branchObjectList = new ArrayList<>();
        warehouseObjectList = new ArrayList<>();
        addStockOpnameObjectList = new ArrayList<>();
        detailStockOpnameObjectList = new ArrayList<>();
        branchList = new ArrayList<>();
        warehouseList = new ArrayList<>();
        branchIdList = new ArrayList<>();
        warehouseIdList = new ArrayList<>();

        PreferenceManagers.clearDataWithKey(Config.KEY_WAREHOUSE_INPUT, this);
        PreferenceManagers.clearDataWithKey(Config.KEY_BRANCH_INPUT, this);
        PreferenceManagers.clearDataWithKey(Config.KEY_DETAIL_SO_INPUT, this);
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            if (actBranch != null && actWarehouse != null) {
                actBranch.setFocusableInTouchMode(false);
                actBranch.setClickable(false);
                actBranch.setFocusable(false);
                actWarehouse.setFocusableInTouchMode(false);
                actWarehouse.setClickable(false);
                actWarehouse.setFocusable(false);
            }
            hasExtra = true;
            String stockOpnameData = intent.getStringExtra(Config.KEY_ALL_STOCK_OPNAME_JSON);
            Timber.i(stockOpnameData);
            Gson gson = new Gson();
            stockOpnameDataObject = gson.fromJson(stockOpnameData, StockOpnameDataObject.class);
        }

        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        actDate.setText(dateFormat.format(date));
        inputDate = dateFormat.format(date);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                barcodeScanUtils.CheckPermission(getActivity());
            }
        });
    }

    @Override
    protected Activity getActivity() {
        return this;
    }

    @Override
    protected String getTvTitle() {
        return "Batch Details";
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_input_stock_opname;
    }

    @Override
    protected Boolean isShowAddToolbar() {
        return false;
    }

    @Override
    protected Boolean isShowNextToolbar() {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Timber.d("Scan canceled");
            } else {
                getProductByItem(branchID, warehouseID, result.getContents());
                Timber.d(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected void getBranch() {
        dialog.show();

        branchID = PreferenceManagers.getData(Config.KEY_ID_BRANCH, mContext);
        PreferenceManagers.setDataWithSameKey(Config.KEY_BRANCH_INPUT, branchID, mContext);
        getWarehouse(branchID);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PreferenceManagers.hasData(Config.KEY_INPUT_MANUAL, mContext)) {
            PreferenceManagers.clearDataWithKey(Config.KEY_INPUT_MANUAL, mContext);
            inputStockOpnameDialog.displayInputDialog(getActivity(), null, null, postAddStockOpnameCallback, null, null);
        }
        databaseHelper = new DatabaseHelper(this);
        int aso_max_api_id = databaseHelper.getMaxIdASO();
        //int aso_api_id = aso_max_api_id + 1;
        if (hasExtra) {
            dataDStockOpname = databaseHelper.getTableDetailStockOpnameArray(stockOpnameDataObject.getId(), true);
        } else {
            dataDStockOpname = databaseHelper.getTableDetailStockOpnameArray(String.valueOf(aso_max_api_id), true);
        }

        tableViewAddStockOpname.setColumnCount(6);
        tableViewAddStockOpname.setColumnWeight(0, 5);
        tableViewAddStockOpname.setColumnWeight(1, 10);
        tableViewAddStockOpname.setColumnWeight(2, 7);
        tableViewAddStockOpname.setColumnWeight(3, 3);
        tableViewAddStockOpname.setColumnWeight(4, 3);
        tableViewAddStockOpname.setColumnWeight(5, 6);
        tableViewAddStockOpname.setHeaderBackgroundColor(Color.parseColor("#FFC107"));
        tableViewAddStockOpname.setHeaderAdapter(new CustomTableHeaderAdapter(this, headerDStockOpname));
        tableViewAddStockOpname.setDataAdapter(new CustomTableDataAdapter(this, dataDStockOpname));
        tableViewAddStockOpname.addDataClickListener(new TableDataClickListener<String[]>() {
            @Override
            public void onDataClicked(int rowIndex, String[] clickedData) {
                Toast.makeText(DetailStockOpnameActivity.this, clickedData[0], Toast.LENGTH_SHORT).show();
            }
        });

        Boolean actWarehouseBool = PreferenceManagers.hasData(Config.KEY_WAREHOUSE_INPUT, getActivity());
        if (actWarehouseBool) {
            actWarehouse.setFocusableInTouchMode(false);
            actWarehouse.setClickable(false);
            actWarehouse.setFocusable(false);
            actWarehouse.setKeyListener(null);
            btnSubmit.setVisibility(View.VISIBLE);
        }
        getBranch();
    }

    protected void getWarehouse(String branch) {

        ApiUtils.getWarehouseAPI(mActivity, Integer.parseInt(branch), new BaseApiService.GetWarehouseCallback() {
            @Override
            public void onSuccess(@NonNull List<WarehouseObject> value) {
                warehouseObjectList = value;
                if (warehouseList != null) {
                    for (int i = 0; i < warehouseObjectList.size(); i++) {
                        warehouseList.add(warehouseObjectList.get(i).getName());
                        warehouseIdList.add(String.valueOf(warehouseObjectList.get(i).getId()));
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<>
                            (mContext, android.R.layout.select_dialog_item, warehouseList);
                    //Getting the instance of AutoCompleteTextView
                    actWarehouse.setThreshold(1);//will start working from first character
                    actWarehouse.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                    adapter.notifyDataSetChanged();

                    actWarehouse.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String warehouseSelected = parent.getItemAtPosition(position).toString();
                            int indexSelected = warehouseList.indexOf(warehouseSelected);
                            warehouseID = warehouseIdList.get(indexSelected);
                            mWarehouse = warehouseList.get(position);
                            PreferenceManagers.setDataWithSameKey(Config.KEY_WAREHOUSE_INPUT, warehouseID, mContext);
                        }
                    });
                    if (hasExtra) {
                        actWarehouse.setText(stockOpnameDataObject.getWarehouse());
                        actWarehouse.performCompletion();
                        PreferenceManagers.setDataWithSameKey(Config.KEY_WAREHOUSE_INPUT, warehouseID, mContext);
//                                new DetailStockOpnameAsycTask().execute("listDetail");

                        ApiUtils.getDetailStockOpname(mActivity, stockOpnameDataObject.getId(), new BaseApiService.GetDetailStockOpnameCallback() {
                            @Override
                            public void onSuccess(@NonNull List<DetailStockOpnameObject> value) {
                                if (value != null) {
                                    if (value.size() > 0) {
                                        detailStockOpnameObjectList = value;

                                        databaseHelper.dropDetail_Stock_Opname_Table();
                                        for (int i = 0; i < detailStockOpnameObjectList.size(); i++) {
                                            databaseHelper.insertDetailStockOpname(detailStockOpnameObjectList.get(i).getId(),
                                                    detailStockOpnameObjectList.get(i).getStock_opname_id(),
                                                    detailStockOpnameObjectList.get(i).getProduct_id(),
                                                    detailStockOpnameObjectList.get(i).getQty(),
                                                    detailStockOpnameObjectList.get(i).getQty(),
                                                    detailStockOpnameObjectList.get(i).getDeference(),
                                                    detailStockOpnameObjectList.get(i).getItem_no(),
                                                    detailStockOpnameObjectList.get(i).getDeleted_at(),
                                                    detailStockOpnameObjectList.get(i).getName(),
                                                    detailStockOpnameObjectList.get(i).getCode(),
                                                    detailStockOpnameObjectList.get(i).getWarehouse_id(),
                                                    "",
                                                    detailStockOpnameObjectList.get(i).getBranch_id(),
                                                    detailStockOpnameObjectList.get(i).getUnit_id(),
                                                    detailStockOpnameObjectList.get(i).getUnit_conversion(),
                                                    detailStockOpnameObjectList.get(i).getUnitname(),
                                                    "true");
                                        }
                                        tableViewAddStockOpname.setColumnCount(6);
                                        tableViewAddStockOpname.setColumnWeight(0, 5);
                                        tableViewAddStockOpname.setColumnWeight(1, 10);
                                        tableViewAddStockOpname.setColumnWeight(2, 7);
                                        tableViewAddStockOpname.setColumnWeight(3, 3);
                                        tableViewAddStockOpname.setColumnWeight(4, 3);
                                        tableViewAddStockOpname.setColumnWeight(5, 6);
                                        tableViewAddStockOpname.setHeaderBackgroundColor(Color.parseColor("#FFC107"));
                                        dataDStockOpname = databaseHelper.getTableDetailStockOpnameArray(stockOpnameDataObject.getId(),true);
                                        tableViewAddStockOpname.setHeaderAdapter(new CustomTableHeaderAdapter(mActivity, headerDStockOpname));
                                        tableViewAddStockOpname.setDataAdapter(new CustomTableDataAdapter(mActivity, dataDStockOpname));
                                        tableViewAddStockOpname.addDataClickListener(new TableDataClickListener<String[]>() {
                                            @Override
                                            public void onDataClicked(int rowIndex, String[] clickedData) {
                                                Toast.makeText(DetailStockOpnameActivity.this, clickedData[0], Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    } else {
                                        tableViewAddStockOpname.setColumnCount(6);
                                        tableViewAddStockOpname.setColumnWeight(0, 5);
                                        tableViewAddStockOpname.setColumnWeight(1, 10);
                                        tableViewAddStockOpname.setColumnWeight(2, 7);
                                        tableViewAddStockOpname.setColumnWeight(3, 3);
                                        tableViewAddStockOpname.setColumnWeight(4, 3);
                                        tableViewAddStockOpname.setColumnWeight(5, 6);
                                        tableViewAddStockOpname.setHeaderBackgroundColor(Color.parseColor("#FFC107"));
                                        dataDStockOpname = databaseHelper.getTableDetailStockOpnameArray(stockOpnameDataObject.getId(), true);
                                        tableViewAddStockOpname.setHeaderAdapter(new CustomTableHeaderAdapter(mActivity, headerDStockOpname));
                                        tableViewAddStockOpname.setDataAdapter(new CustomTableDataAdapter(mActivity, dataDStockOpname));
                                        tableViewAddStockOpname.addDataClickListener(new TableDataClickListener<String[]>() {
                                            @Override
                                            public void onDataClicked(int rowIndex, String[] clickedData) {
                                                Toast.makeText(DetailStockOpnameActivity.this, clickedData[0], Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        Timber.e("Gagal mengambil data detail stock opname - 1 (" + value + ")");
                                    }
                                } else {
                                    tableViewAddStockOpname.setColumnCount(6);
                                    tableViewAddStockOpname.setColumnWeight(0, 5);
                                    tableViewAddStockOpname.setColumnWeight(1, 10);
                                    tableViewAddStockOpname.setColumnWeight(2, 7);
                                    tableViewAddStockOpname.setColumnWeight(3, 3);
                                    tableViewAddStockOpname.setColumnWeight(4, 3);
                                    tableViewAddStockOpname.setColumnWeight(5, 6);
                                    tableViewAddStockOpname.setHeaderBackgroundColor(Color.parseColor("#FFC107"));
                                    dataDStockOpname = databaseHelper.getTableDetailStockOpnameArray(stockOpnameDataObject.getId(), true);
                                    tableViewAddStockOpname.setHeaderAdapter(new CustomTableHeaderAdapter(mActivity, headerDStockOpname));
                                    tableViewAddStockOpname.setDataAdapter(new CustomTableDataAdapter(mActivity, dataDStockOpname));
                                    tableViewAddStockOpname.addDataClickListener(new TableDataClickListener<String[]>() {
                                        @Override
                                        public void onDataClicked(int rowIndex, String[] clickedData) {
                                            Toast.makeText(DetailStockOpnameActivity.this, clickedData[0], Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                    Timber.e("Gagal mengambil data detail stock opname - 2 (" + value + ")");

                                    dialogMessage.displayMessageIntentHome(DetailStockOpnameActivity.this, "Error", "data not found");
                                }
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            }

                            @Override
                            public void onError(@NonNull Throwable t) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                dialogMessage.displayMessage(DetailStockOpnameActivity.this, "Error", t.getMessage());
                                Timber.e("Gagal mengambil data detail stock opname - 2 (" + t.getMessage() + ")");
                            }
                        });
                    } else {
                        if (dialog.isShowing())
                            dialog.dismiss();
                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                getWarehouse(branchID);
            }
        });
    }

    protected void getProductByItem(final String branch, final String warehouse, final String itemNo) {
        dialog.show();

        ApiUtils.getdataProductbyItem(mActivity, branch, warehouse, itemNo, new BaseApiService.GetDataProductDetailCallback() {
            @Override
            public void onSuccess(@NonNull List<AddStockOpnameObject> value) {
                if (value != null) {
                    value.get(0).setDate(inputDate);
                    value.get(0).setWarehouse(mWarehouse);
                    value.get(0).setWarehouse_id(warehouse);
                    value.get(0).setBranch_id(branch);
                    String addStockOpnameObjectJson = (new Gson().toJson(value));
                    PreferenceManagers.setDataWithSameKey(Config.KEY_DETAIL_SO_INPUT, addStockOpnameObjectJson, mContext);

                    final AddStockOpnameObject addStockOpnameObject = value.get(0);
                    ApiUtils.GetUnit(mActivity, itemNo, new BaseApiService.GetUnitCallback() {
                        @Override
                        public void onSuccess(@NonNull List<UnitObject> value) {
                            inputStockOpnameDialog.displayInputDialog(mActivity, addStockOpnameObject, value, postAddStockOpnameCallback, null, null);
                        }

                        @Override
                        public void onError(@NonNull Throwable throwable) {

                        }
                    });
                } else {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    dialogMessage.displayMessage(DetailStockOpnameActivity.this, "Error", "No data found");
                    Timber.e("Gagal mengambil getdataProductbyItem - 1 (" + value + ")");
                }
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                dialogMessage.displayMessage(DetailStockOpnameActivity.this, "Error", throwable.getMessage());
                Timber.e("Gagal mengambil getdataProductbyItem - 2 (" + throwable.getMessage() + ")");
            }
        });
    }
}
