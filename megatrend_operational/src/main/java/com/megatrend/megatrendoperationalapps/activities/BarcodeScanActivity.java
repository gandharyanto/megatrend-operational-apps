package com.megatrend.megatrendoperationalapps.activities;

import android.view.View;
import android.widget.TextView;

import com.journeyapps.barcodescanner.CaptureActivity;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.megatrend.megatrendoperationalapps.R;
import com.megatrend.megatrendoperationalapps.utils.Config;
import com.megatrend.megatrendoperationalapps.utils.PreferenceManagers;

public class BarcodeScanActivity extends CaptureActivity {
    @Override
    protected DecoratedBarcodeView initializeContent() {
        setContentView(R.layout.activity_barcode_scan);
        TextView tvInputManual = findViewById(R.id.tvInputManual);
        tvInputManual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManagers.setDataWithSameKey(Config.KEY_INPUT_MANUAL, "true", getApplicationContext());
                finish();
            }
        });
        return (DecoratedBarcodeView) findViewById(R.id.zxing_barcode_scanner);
    }

}
