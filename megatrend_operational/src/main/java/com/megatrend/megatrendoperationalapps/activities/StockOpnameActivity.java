package com.megatrend.megatrendoperationalapps.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.megatrend.megatrendoperationalapps.R;
import com.megatrend.megatrendoperationalapps.adapter.CustomTableDataAdapter;
import com.megatrend.megatrendoperationalapps.adapter.CustomTableHeaderAdapter;
import com.megatrend.megatrendoperationalapps.component.ProgressDialogManager;
import com.megatrend.megatrendoperationalapps.db.DatabaseHelper;
import com.megatrend.megatrendoperationalapps.model.ResponsePostGetSoOpen;
import com.megatrend.megatrendoperationalapps.object.GetSoOpenObject;
import com.megatrend.megatrendoperationalapps.object.StockOpnameDataObject;
import com.megatrend.megatrendoperationalapps.object.WarehouseObject;
import com.megatrend.megatrendoperationalapps.utils.Config;
import com.megatrend.megatrendoperationalapps.utils.DialogViews;
import com.megatrend.megatrendoperationalapps.utils.PreferenceManagers;
import com.megatrend.megatrendoperationalapps.utils.api.ApiUtils;
import com.megatrend.megatrendoperationalapps.utils.api.BaseApiService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.listeners.TableDataClickListener;

public class StockOpnameActivity extends BaseActivity {

    String[] headerStockOpname = {"No.", "Date", "Warehouse", "Created By"};
    String[][] dataStockOpname;
    @BindView(R.id.tableViewAllStockOpname)
    TableView<String[]> tableViewAllStockOpname;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swiperefresh;
    Context mContext;
    Activity mActivity;
    List<StockOpnameDataObject> stockOpnameDataObjectList;
    List<StockOpnameDataObject> stockOpnameDataObjectList1;
    StockOpnameDataObject stockOpnameDataObject;
    BaseApiService mApiService;
    DialogViews.DialogMessage dialogMessage = new DialogViews.DialogMessage();
    String branch_id;
    ProgressDialogManager dialog;
    private DatabaseHelper databaseHelper;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(getActivity(), MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mContext = this;
        mActivity = this;

        dialog = new ProgressDialogManager(this);
        mApiService = ApiUtils.getAPIService();
        databaseHelper = new DatabaseHelper(this);
        tableViewAllStockOpname.setColumnCount(4);
        tableViewAllStockOpname.setHeaderBackgroundColor(Color.parseColor("#FFC107"));
        dataStockOpname = databaseHelper.getTableStockOpnameDataArray();
        stockOpnameDataObjectList = new ArrayList<>();
        stockOpnameDataObjectList = databaseHelper.getTableStockOpnameData();
        tableViewAllStockOpname.setHeaderAdapter(new CustomTableHeaderAdapter(this, headerStockOpname));
        tableViewAllStockOpname.setDataAdapter(new CustomTableDataAdapter(this, dataStockOpname));

        branch_id = PreferenceManagers.getData(Config.KEY_ID_BRANCH, mContext);

        tableViewAllStockOpname.addDataClickListener(new TableDataClickListener<String[]>() {
            @Override
            public void onDataClicked(int rowIndex, String[] clickedData) {
                int position = -1;
                for (int i = 0; i < dataStockOpname.length; i++) {
                    if (dataStockOpname[i][0] == clickedData[0]) {
                        position = i;
                    }
                }
                if (position != -1) {
                    stockOpnameDataObject = stockOpnameDataObjectList.get(position);
                    String allStockOpname = (new Gson().toJson(stockOpnameDataObject));
                    Intent intent = new Intent(StockOpnameActivity.this, DetailStockOpnameActivity.class);
                    intent.putExtra(Config.KEY_ALL_STOCK_OPNAME_JSON, allStockOpname);
                    startActivity(intent);
                }
            }
        });

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getStockOpname();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getStockOpname();
    }

    @Override
    protected Activity getActivity() {
        return this;
    }

    @Override
    protected String getTvTitle() {
        return "Stock Opname";
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_all_stock_opname;
    }

    @Override
    protected Boolean isShowAddToolbar() {
        return true;
    }

    @Override
    protected Boolean isShowNextToolbar() {
        return false;
    }

    protected void getStockOpname() {
        dialog.show();

        ApiUtils.getStockOpname(mActivity, new BaseApiService.GetStockOpnameCallback() {
            @Override
            public void onSuccess(@NonNull List<StockOpnameDataObject> value) {
                stockOpnameDataObjectList1 = value;
                if (stockOpnameDataObjectList1 != null) {
                    databaseHelper.dropAll_Table();
                    for (int i = 0; i < stockOpnameDataObjectList1.size(); i++) {
                        databaseHelper.insertAllStockOpname(stockOpnameDataObjectList1.get(i).getId(),
                                stockOpnameDataObjectList1.get(i).getDate(),
                                stockOpnameDataObjectList1.get(i).getCreated_at(),
                                stockOpnameDataObjectList1.get(i).getUpdated_by(),
                                stockOpnameDataObjectList1.get(i).getUpdated_at(),
                                stockOpnameDataObjectList1.get(i).getWarehouse_id(),
                                stockOpnameDataObjectList1.get(i).getBranch_id(),
                                stockOpnameDataObjectList1.get(i).getDeleted_at(),
                                stockOpnameDataObjectList1.get(i).getWarehouse(),
                                "true",
                                stockOpnameDataObjectList1.get(i).getCode());
//                        }
                    }

                    stockOpnameDataObjectList = databaseHelper.getTableStockOpnameData();
                } else {
                    if (dialog.isShowing())
                        dialog.dismiss();
                    dialogMessage.displayMessage(mActivity, "Error", "tidak ada");
                }

                tableViewAllStockOpname.setColumnCount(4);
                tableViewAllStockOpname.setHeaderBackgroundColor(Color.parseColor("#FFC107"));
                dataStockOpname = databaseHelper.getTableStockOpnameDataArray();
                tableViewAllStockOpname.setHeaderAdapter(new CustomTableHeaderAdapter(mActivity, headerStockOpname));
                tableViewAllStockOpname.setDataAdapter(new CustomTableDataAdapter(mActivity, dataStockOpname));
                swiperefresh.setRefreshing(false);
                getWarehouse(branch_id);
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                if (dialog.isShowing())
                    dialog.dismiss();
                dialogMessage.displayMessage(mActivity, "Error", throwable.getMessage());
            }
        });
    }

    protected void getWarehouse(final String branch_id) {
        ApiUtils.getWarehouseAPI(mActivity, Integer.parseInt(branch_id), new BaseApiService.GetWarehouseCallback() {
            @Override
            public void onSuccess(@NonNull List<WarehouseObject> value) {
                List<WarehouseObject> warehouseObjectList = value;
                if (warehouseObjectList.size() != 0) {
                    for (int i = 0; i < warehouseObjectList.size(); i++) {
                        databaseHelper.insertWarehouse(String.valueOf(warehouseObjectList.get(i).getId()),
                                warehouseObjectList.get(i).getBranch_id(),
                                warehouseObjectList.get(i).getCode(),
                                warehouseObjectList.get(i).getName(),
                                warehouseObjectList.get(i).getDescription(),
                                warehouseObjectList.get(i).getEmployee_id(),
                                warehouseObjectList.get(i).getAddress(),
                                warehouseObjectList.get(i).getArea_post_code_id());
                    }
                    getSoOpen();
                } else {
                    getSoOpen();
                }
            }

            @Override
            public void onError(@NonNull Throwable throwable) {
                getWarehouse(branch_id);
            }
        });
    }

    protected void getSoOpen() {
        final List<WarehouseObject> warehouseObjectList = databaseHelper.getWarehouseData(PreferenceManagers.getData(Config.KEY_ID_BRANCH, mActivity));

        for (int i = 0; i < warehouseObjectList.size(); i++) {
            final Map<String, String> data = new HashMap<>();
            data.put("branch_id", PreferenceManagers.getData(Config.KEY_ID_BRANCH, mActivity));
            data.put("warehouse_id", String.valueOf(warehouseObjectList.get(i).getId()));
            data.put("updated_by", PreferenceManagers.getData(Config.KEY_ID_SALESMAN, mActivity));

            ApiUtils.PostGetSOOpen(mActivity, data, new BaseApiService.PostGetSOOpenCallback() {
                @Override
                public void onSuccess(@NonNull ResponsePostGetSoOpen value) {
                    List<GetSoOpenObject> getSoOpenObjects = value.getGetSoOpenObjects();
                    if (getSoOpenObjects.size() != 0) {
                        for (int i = 0; i < getSoOpenObjects.size(); i++) {
                            databaseHelper.insertSOOpen(getSoOpenObjects.get(i).getId(),
                                    getSoOpenObjects.get(i).getCode(),
                                    getSoOpenObjects.get(i).getStatus(),
                                    getSoOpenObjects.get(i).getDate());
                        }
                    }
                    if (dialog.isShowing())
                        dialog.dismiss();
                }

                @Override
                public void onError(@NonNull Throwable throwable) {
                    getSoOpen();
                }
            });
        }
    }
}