package com.megatrend.megatrendoperationalapps.activities;

import android.app.Activity;

import com.megatrend.megatrendoperationalapps.R;

public class DriverSignActivity extends BaseActivity{
    @Override
    protected Activity getActivity() {
        return this;
    }

    @Override
    protected String getTvTitle() {
        return "Signature";
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_driver_sign;
    }

    @Override
    protected Boolean isShowAddToolbar() {
        return false;
    }

    @Override
    protected Boolean isShowNextToolbar() {
        return false;
    }
}
