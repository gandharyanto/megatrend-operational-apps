package com.megatrend.megatrendoperationalapps.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.Time;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.megatrend.megatrendoperationalapps.R;
import com.megatrend.megatrendoperationalapps.adapter.AllWarehouseAdapter;
import com.megatrend.megatrendoperationalapps.adapter.SoOpenDetailAdapter;
import com.megatrend.megatrendoperationalapps.component.ProgressDialogManager;
import com.megatrend.megatrendoperationalapps.db.DatabaseHelper;
import com.megatrend.megatrendoperationalapps.model.ResponsePostChangeStatus;
import com.megatrend.megatrendoperationalapps.model.ResponsePostCreateSTO;
import com.megatrend.megatrendoperationalapps.model.ResponsePostGetSoOpen;
import com.megatrend.megatrendoperationalapps.object.AddStockOpnameObject;
import com.megatrend.megatrendoperationalapps.object.GetSoOpenObject;
import com.megatrend.megatrendoperationalapps.object.SoOpenDetailObject;
import com.megatrend.megatrendoperationalapps.object.WarehouseObject;
import com.megatrend.megatrendoperationalapps.utils.BarcodeScanUtils;
import com.megatrend.megatrendoperationalapps.utils.Config;
import com.megatrend.megatrendoperationalapps.utils.DialogViews;
import com.megatrend.megatrendoperationalapps.utils.PreferenceManagers;
import com.megatrend.megatrendoperationalapps.utils.RecyclerItemClickListener;
import com.megatrend.megatrendoperationalapps.utils.api.ApiUtils;
import com.megatrend.megatrendoperationalapps.utils.api.BaseApiService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by user on 1/16/2018.
 */

public abstract class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Nullable
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @Nullable
    @BindView(R.id.tvNext)
    TextView tvNext;
    @Nullable
    @BindView(R.id.tvSubmit)
    TextView tvSubmit;
    @Nullable
    @BindView(R.id.tvLogOut)
    TextView tvLogOut;
    @Nullable
    @BindView(R.id.ibAdd)
    ImageView ibAdd;
    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @Nullable
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @Nullable
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    static DatabaseHelper databaseHelper;
    DialogViews.DialogMessage dialogMessage = new DialogViews.DialogMessage();
    static List<SoOpenDetailObject> soOpenDetailObjectList = new ArrayList<>();
    BarcodeScanUtils.CheckPermission barcodeScanUtils = new BarcodeScanUtils.CheckPermission();
    DialogViews.InputStockOpnameDialog inputStockOpnameDialog = new DialogViews.InputStockOpnameDialog();
    ProgressDialogManager loadingDialog;
    String myAct;
    BaseApiService.PostAddStockOpnameCallback postAddStockOpnameCallback = new BaseApiService.PostAddStockOpnameCallback() {

        @Override
        public void onSuccess(@NonNull String value) {
            //Timber.d(value);
            dialogMessage.displayMessage(getActivity(), value, "");
        }

        @Override
        public void onError(@NonNull Throwable throwable) {
            dialogMessage.displayMessage(getActivity(), "Fail", "");
        }
    };

    public void displayDialog(final Activity act) {
        final Dialog dialog = new Dialog(act);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_create_menu);

        final Button btnCreate = dialog.findViewById(R.id.btnCreate);
        final Button btnInsert = dialog.findViewById(R.id.btnInsert);
        final Button btnClose = dialog.findViewById(R.id.btnClose);
        final RecyclerView rvWarehouse = dialog.findViewById(R.id.rvWarehouse);
        final TextView tvTitleRv = dialog.findViewById(R.id.tvTitleRv);

        final Date date = new Date();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        soOpenDetailObjectList = databaseHelper.getSoOpenDetail();

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnCreate.setVisibility(View.GONE);
                btnInsert.setVisibility(View.GONE);
                btnClose.setVisibility(View.GONE);

                if (soOpenDetailObjectList.size() != 0) {
                    SoOpenDetailAdapter soOpenDetailAdapter = new SoOpenDetailAdapter(act, soOpenDetailObjectList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(act);
                    rvWarehouse.setLayoutManager(mLayoutManager);
                    rvWarehouse.setItemAnimator(new DefaultItemAnimator());
                    rvWarehouse.setAdapter(soOpenDetailAdapter);
                    soOpenDetailAdapter.notifyDataSetChanged();
                    rvWarehouse.setVisibility(View.VISIBLE);
                    tvTitleRv.setVisibility(View.VISIBLE);
                    tvTitleRv.setText("Select Stock Opname");
                    rvWarehouse.addOnItemTouchListener(new RecyclerItemClickListener(act, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, final int position) {
                            PreferenceManagers.setDataWithSameKey(Config.KEY_ID_WAREHOUSE, String.valueOf(soOpenDetailObjectList.get(position).getWarehouse_id()), act);
                            PreferenceManagers.setDataWithSameKey(Config.KEY_ID_EMPLOYEE, String.valueOf(soOpenDetailObjectList.get(position).getEmployee_id()), act);
                            PreferenceManagers.setDataWithSameKey(Config.KEY_ID_API_ASO, String.valueOf(soOpenDetailObjectList.get(position).getApi_id()), act);

                            final Map<String, String> data = new HashMap<>();
                            final String code = soOpenDetailObjectList.get(position).getCode();
                            data.put("stock_opname_id", soOpenDetailObjectList.get(position).getApi_id());
                            data.put("updated_by", PreferenceManagers.getData(Config.KEY_ID_SALESMAN, act));

                            ApiUtils.postChangeStatus(act, data, new BaseApiService.PostChangeStatusCallback() {
                                @Override
                                public void onSuccess(@NonNull ResponsePostChangeStatus value) {
                                    databaseHelper.deleteCloseSTO(code);
                                    dialogMessage.displayMessage(act, value.getTitle(), value.getMsg());
                                }

                                @Override
                                public void onError(@NonNull Throwable throwable) {

                                }
                            });
                            dialog.dismiss();
                        }
                    }));
                } else {
                    dialog.dismiss();
                    dialogMessage.displayMessage(act, "", "No Open Warehouse");
                }
            }
        });

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCreate.setVisibility(View.GONE);
                btnInsert.setVisibility(View.GONE);
                btnClose.setVisibility(View.GONE);
                final List<WarehouseObject> warehouseObjectList = databaseHelper.getWarehouseData(PreferenceManagers.getData(Config.KEY_ID_BRANCH, act));
                if (warehouseObjectList.size() != 0) {
                    AllWarehouseAdapter allStockOpnameAdapter = new AllWarehouseAdapter(act, warehouseObjectList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(act);
                    rvWarehouse.setLayoutManager(mLayoutManager);
                    rvWarehouse.setItemAnimator(new DefaultItemAnimator());
                    rvWarehouse.setAdapter(allStockOpnameAdapter);
                    allStockOpnameAdapter.notifyDataSetChanged();
                    rvWarehouse.setVisibility(View.VISIBLE);
                    tvTitleRv.setVisibility(View.VISIBLE);
                    tvTitleRv.setText("Select Warehouse");
                    rvWarehouse.addOnItemTouchListener(new RecyclerItemClickListener(act, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            final Map<String, String> data = new HashMap<>();
                            data.put("branch_id", PreferenceManagers.getData(Config.KEY_ID_BRANCH, act));
                            data.put("warehouse_id", String.valueOf(warehouseObjectList.get(position).getId()));
                            data.put("updated_by", PreferenceManagers.getData(Config.KEY_ID_SALESMAN, act));

                            PreferenceManagers.setDataWithSameKey(Config.KEY_ID_WAREHOUSE, String.valueOf(warehouseObjectList.get(position).getId()), act);

                            ApiUtils.PostGetSOOpen(act, data, new BaseApiService.PostGetSOOpenCallback() {
                                @Override
                                public void onSuccess(@NonNull ResponsePostGetSoOpen value) {
                                    List<GetSoOpenObject> getSoOpenObjects = value.getGetSoOpenObjects();
                                    if (getSoOpenObjects.size() != 0) {
                                        for (int i = 0; i < getSoOpenObjects.size(); i++) {
                                            databaseHelper.insertSOOpen(getSoOpenObjects.get(i).getId(),
                                                    getSoOpenObjects.get(i).getCode(),
                                                    getSoOpenObjects.get(i).getStatus(),
                                                    getSoOpenObjects.get(i).getDate());
                                        }
                                        dialogMessage.displayMessage(act, "Error", "Stock opname still open \n Go to Insert Data");
                                    } else {
                                        final Map<String, String> data = new HashMap<>();
                                        data.put("branch_id", PreferenceManagers.getData(Config.KEY_ID_BRANCH, act));
                                        data.put("warehouse_id", PreferenceManagers.getData(Config.KEY_ID_WAREHOUSE, act));
                                        data.put("updated_by", PreferenceManagers.getData(Config.KEY_ID_SALESMAN, act));
                                        data.put("date", dateFormat.format(date));

                                        ApiUtils.PostCreateSTO(act, data, new BaseApiService.PostCreateSTOCallback() {
                                            @Override
                                            public void onSuccess(@NonNull ResponsePostCreateSTO value) {
                                                dialog.dismiss();
                                                Intent intent = new Intent(getActivity(), getActivity().getClass());
                                                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                                getActivity().startActivity(intent);
                                            }

                                            @Override
                                            public void onError(@NonNull Throwable throwable) {

                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onError(@NonNull Throwable throwable) {

                                }
                            });

                        }
                    }));
                }


            }
        });

        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCreate.setVisibility(View.GONE);
                btnInsert.setVisibility(View.GONE);
                btnClose.setVisibility(View.GONE);

                if (soOpenDetailObjectList.size() != 0) {
                    SoOpenDetailAdapter soOpenDetailAdapter = new SoOpenDetailAdapter(act, soOpenDetailObjectList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(act);
                    rvWarehouse.setLayoutManager(mLayoutManager);
                    rvWarehouse.setItemAnimator(new DefaultItemAnimator());
                    rvWarehouse.setAdapter(soOpenDetailAdapter);
                    soOpenDetailAdapter.notifyDataSetChanged();
                    rvWarehouse.setVisibility(View.VISIBLE);
                    tvTitleRv.setVisibility(View.VISIBLE);
                    tvTitleRv.setText("Select Stock Opname");
                    rvWarehouse.addOnItemTouchListener(new RecyclerItemClickListener(act, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            PreferenceManagers.setDataWithSameKey(Config.KEY_ID_WAREHOUSE, String.valueOf(soOpenDetailObjectList.get(position).getWarehouse_id()), act);
                            PreferenceManagers.setDataWithSameKey(Config.KEY_WAREHOUSE_INPUT, String.valueOf(soOpenDetailObjectList.get(position).getWarehouse()), act);
                            PreferenceManagers.setDataWithSameKey(Config.KEY_ID_EMPLOYEE, String.valueOf(soOpenDetailObjectList.get(position).getEmployee_id()), act);
                            PreferenceManagers.setDataWithSameKey(Config.KEY_ID_API_ASO, String.valueOf(soOpenDetailObjectList.get(position).getApi_id()), act);
                            Intent intent = new Intent(act, InputStockOpnameActivity.class);
                            intent.putExtra("status", "2");
                            act.startActivity(intent);
                            dialog.dismiss();
                        }
                    }));
                } else {
                    dialog.dismiss();
                    dialogMessage.displayMessage(act, "", "No Open Warehouse");
                }
            }
        });

        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutResourceId());
        ButterKnife.bind(getActivity());

        setSupportActionBar(toolbar);

        tvTitle.setText(getTvTitle());
        tvTitle.setTypeface(Typeface.DEFAULT_BOLD);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        View v = navigationView.getHeaderView(0);
        TextView userName = v.findViewById(R.id.userName);
        TextView userMail = v.findViewById(R.id.userMail);

        userName.setText(PreferenceManagers.getData(Config.KEY_USERNAME, getActivity()));
        userMail.setText(PreferenceManagers.getData(Config.KEY_USERMAIL, getActivity()));

        navigationView.getMenu().findItem(R.id.nav_driver).setVisible(true);
        navigationView.getMenu().setGroupVisible(R.id.menu_group_admin, true);

        navigationView.setNavigationItemSelectedListener(this);

        databaseHelper = new DatabaseHelper(getActivity());

        if (!isShowAddToolbar()) {
            if (ibAdd != null)
                ibAdd.setVisibility(View.GONE);
        }
        if (!isShowNextToolbar()) {
            if (tvNext != null)
                tvNext.setVisibility(View.GONE);
        } else {
            String actWarehouse = PreferenceManagers.getData(Config.KEY_WAREHOUSE_INPUT, getActivity());
            Boolean actWarehouseBool = PreferenceManagers.hasData(Config.KEY_WAREHOUSE_INPUT, getActivity());
            if (actWarehouseBool) {
                if (tvSubmit != null)
                    tvSubmit.setVisibility(View.VISIBLE);
                if (tvNext != null)
                    tvNext.setVisibility(View.GONE);

                Intent intent = new Intent(getActivity(), getActivity().getClass());
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                getActivity().startActivity(intent);
            }
        }
        myAct = getActivity().getClass().getName();

        if (tvLogOut != null)
            tvLogOut.setVisibility(View.GONE);
        loadingDialog = new ProgressDialogManager(getActivity());

        /*int lastDate = Integer.parseInt(PreferenceManagers.getData(Config.KEY_LAST_LOGIN, getActivity()));
        //int lastDate = 0;
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        int now = today.monthDay;

        if (lastDate != now) {*/

        if (tvLogOut != null)
            tvLogOut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (myAct.equalsIgnoreCase("com.megatrend.megatrendoperationalapps.activities.StockOpnameActivity")) {
                        Intent i = new Intent(getActivity(), MainActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getActivity().startActivity(i);
                    }
                }
            });

//        }

        if (ibAdd != null)
            ibAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PreferenceManagers.setDataWithSameKey(Config.KEY_UPDATE, "false", getActivity());

                    if (myAct.equalsIgnoreCase("com.megatrend.megatrendoperationalapps.activities.StockOpnameActivity")) {
//                    if (PreferenceManagers.getData(Config.KEY_USERNAME, getActivity()).equalsIgnoreCase("yudhy.budiman@megatrend.co.id")) {
                        displayDialog(getActivity());
                    /*} else {
                        Intent intent = new Intent(getActivity(), InputStockOpnameActivity.class);
                        startActivity(intent);
                    }*/
                    } else if (myAct.equalsIgnoreCase("com.megatrend.megatrendoperationalapps.activities.InputStockOpnameActivity")) {
                        String actBranch = PreferenceManagers.getData(Config.KEY_BRANCH_INPUT, getActivity());
                        Boolean actBranchBool = PreferenceManagers.hasData(Config.KEY_BRANCH_INPUT, getActivity());
                        String actWarehouse = PreferenceManagers.getData(Config.KEY_WAREHOUSE_INPUT, getActivity());
                        Boolean actWarehouseBool = PreferenceManagers.hasData(Config.KEY_WAREHOUSE_INPUT, getActivity());
                        if (actBranchBool && actWarehouseBool) {
                            PreferenceManagers.setDataWithSameKey(Config.KEY_ADD, "true", getActivity());
                            PreferenceManagers.setDataWithSameKey(Config.KEY_UPDATE, "false", getActivity());
                            String addStockOpnameObjectJson = PreferenceManagers.getData(Config.KEY_DETAIL_SO_INPUT, getActivity());
                            AddStockOpnameObject addStockOpnameObject = (new Gson().fromJson(addStockOpnameObjectJson, AddStockOpnameObject.class));
                            inputStockOpnameDialog.displayInputDialog(getActivity(), addStockOpnameObject, null, postAddStockOpnameCallback, null, null);
                        } else {
                            dialogMessage.displayMessage(getActivity(), "Error", "Please fill branch and warehouse");
                        }
                    }
                }
            });

        if (tvNext != null)
            tvNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String actWarehouse = PreferenceManagers.getData(Config.KEY_WAREHOUSE_INPUT, getActivity());
                    Boolean actWarehouseBool = PreferenceManagers.hasData(Config.KEY_WAREHOUSE_INPUT, getActivity());
                    if (actWarehouseBool) {
                        tvSubmit.setVisibility(View.VISIBLE);
                        tvNext.setVisibility(View.GONE);

                        Intent intent = new Intent(getActivity(), getActivity().getClass());
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        getActivity().startActivity(intent);
                    }
                }
            });

        if (tvSubmit != null)
            tvSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InputStockOpnameActivity inputStockOpnameActivity = new InputStockOpnameActivity();
                    inputStockOpnameActivity.submitStockOpname(getActivity(), getActivity());
                }
            });
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_stock_opname) {
            DatabaseHelper.exportDB(getActivity());
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            PreferenceManagers.setDataWithSameKey(Config.KEY_LAST_LOGIN, today.monthDay + "", getActivity());

            Intent intent = new Intent(getActivity(), StockOpnameActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }else if (id == R.id.nav_driver) {
            Intent intent = new Intent(getActivity(), DriverSignActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (id == R.id.logout) {
            PreferenceManagers.clear(getActivity());

            DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
            databaseHelper.dropAll_Table();
            databaseHelper.dropProduct();

            Intent i = new Intent(getActivity(), LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getActivity().startActivity(i);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    protected abstract Activity getActivity();

    protected abstract String getTvTitle();

    protected abstract int getLayoutResourceId();

    protected abstract Boolean isShowAddToolbar();

    protected abstract Boolean isShowNextToolbar();
}
